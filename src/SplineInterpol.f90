module SplineInterpol
  !
  !**PURPOSE
  !  -------
  ! The MODULE SplineInterpol contains routines and functions related to 
  ! spline interpolation (cspline and bspline).
  !
  !**DESCRIPTION
  !  ------------
  ! The following subroutines (sbr) and functions (fct) are part of 
  ! this module:
  !
  !  -> (fct) eval_cspline_object : evaluate function at given point by interpolation
  !                                "t_spline" object is passed
  !  -> (fct) eval_cspline : evaluate function at given point by interpolation
  !  -> (sbr) init_cspline_object : initialize cspline routine/arrays
  !                                "t_spline" object is passed
  !  -> (sbr) init_cspline : initialize cspline routine/arrays
  !  -> (fct) eval_bezier  : evaluate besier spline interpolation at given point
  !
  !**AUTHOR
  !  ------
  ! passed to Stefan Pabst by Robin Santra (origin of procedures is unkown)
  ! rewritten into a module by Stefan Pabst in April 2011
  !
  !**VERSION
  ! svn info: $Id: SplineInterpol.f90 684 2012-12-24 13:17:01Z spabst $
  ! ----------------------------------------------------------------------
  !
  use Constants, dp=>dblprec 
  implicit none

  type t_cspline
    ! number of function stored in y
    integer :: nfct = 0

    ! curve f_n(x), n represents the 2. index in y [1...nfct]
    real(dp),allocatable :: x(:),y(:,:)
    
    ! spine array
    !  - 1. index : size of argument (= size of x)
    !  - 2. index : spline field [1...3]
    !  - 3. index : function ID [1...nfct]
    real(dp), allocatable :: cspline(:,:,:)

    ! file name
    character(clen) :: file = ""
    logical         :: active = .false.
  end type t_cspline


contains


  pure function eval_cspline_object(a_x0,a_this,ao_fct) Result(res)
    !
    ! user friendly interface to eval_cspline for one function stored in
    ! cspline object (normally 1. function is used)
    !
    !**INPUT/OUTPUT
    !  -------------
    !
    !  a_x0    :  x value to be evaluated
    !
    !  a_this  :  spline interpolation object containing all the infos
    !
    !  ao_fct  :  function ID in spline object
    !             default : 1
    !
    implicit none

    type(t_cspline),intent(in) :: a_this
    real(dp), intent(in) :: a_x0
    integer,intent(in),optional :: ao_fct
    real(dp) :: res

    integer :: fct


    res = ZERO

    ! default 1. function
    fct = 1
    if (present(ao_fct)) fct = ao_fct

    ! check consistency
    if ( .not.allocated(a_this%x) .or. .not.allocated(a_this%y) .or.  &
         .not.allocated(a_this%cspline)                                &
       ) then

      return
      !write(STDERR,*) " ERROR(eval_cspline): cspline object is not allocated"
    else
      if ( size(a_this%cspline,3) < fct ) return
      !write(STDERR,*) " ERROR(eval_cspline): number of fct. in object is too small"
    end if

    res = eval_cspline( a_x0,a_this%x,a_this%y(:,fct),a_this%cspline(:,:,fct) )
    
  end function eval_cspline_object


  pure function eval_cspline(a_x0,a_x,a_y,a_cspline) Result(res)
    ! this subroutine evaluates the cubic spline function
    !
    !    seval = y(i) + b(i)*(u-x(i)) + c(i)*(u-x(i))**2 + d(i)*(u-x(i))**3
    !
    !    where  x(i) .lt. u .lt. x(i+1), using horner's rule
    !
    !  if  x0 <  x(1) then  i = 1  is used.
    !  if  x0 >= x(n) then  i = n  is used.
    
    !
    ! INPUT
    !   x0     : argument at which the cspline interpolation should be evaluated
    !   x,y    : arrays of argument and function values 
    !   cspline: auxiliary cspline fields 
    !            cspline(:,1) = b
    !            cspline(:,2) = c
    !            cspline(:,3) = d
    !
    implicit none

    real(dp),intent(in) ::  a_x0,a_x(:),a_y(:)
    real(dp),intent(in) ::  a_cspline(:,:)
    integer :: i, j, k,sz(2)
    real(dp) :: dx(3),res

    res = ZERO
    
    sz = shape(a_cspline)
    if (sz(2) /= 3 ) return
    if (sz(1) /= size(a_x) .or. sz(1) /= size(a_y)) return

    if (a_x0 <= a_x(1)) then
      i = 1
    else if (a_x0 >= a_x(sz(1))) then
      i = sz(1)
    else
      ! find segment
      i = 1
      j = sz(1) + 1 
      do while (j>i+1)
        k = (i+j)/2
        if ( a_x0 <  a_x(k) ) j = k
        if ( a_x0 >= a_x(k) ) i = k
      end do 
    end if

    ! evaluate cspline
    dx(1) = a_x0 - a_x(i)
    dx(2) = dx(1)**2
    dx(3) = dx(1)**3

    res = a_y(i) + sum(dx*a_cspline(i,:))
    return
  end function eval_cspline



  subroutine init_cspline_object(a_this,ao_info)
    !
    ! user-friendly interface to init_cspline for all functions stored in
    ! cspline object
    !
    !**INPUT/OUTPUT
    !  -------------
    !
    !  a_this  :  TYPE(t_cspline) - INOUT
    !             cspline interpolation object containing all the infos/arrys
    !
    !  ao_info :  LOGICAL - OUT - OPTIONAL
    !              true : successful finished
    !              false: unsuccessful finished
    !
    type(t_cspline),intent(inout) :: a_this
    logical,intent(out),optional :: ao_info

    integer :: i,nx,ny


    ! optional output info
    if (present(ao_info)) ao_info = .false.


    ! check x and y fields exist
    if ( .not.allocated(a_this%x) )             &
        stop " ERROR(init_cspline_obj): x-field is not allocated"
    
    if ( .not.allocated(a_this%y) )             &
        stop " ERROR(init_cspline_obj): y-field is not allocated"
    
    if ( size(a_this%y,1) /= size(a_this%x) )   &
        stop " ERROR(init_cspline_obj): x,y-fields have different lengths"
          
    nx = size(a_this%x)
    ny = size(a_this%y,2)


    ! check if cspline field is allocated/large enough
    if ( allocated(a_this%cspline) .and. ( size(a_this%cspline,1)<nx     &
          .or. size(a_this%cspline,3)<ny .or. size(a_this%cspline,2)<3 ) &
       ) then

      deallocate(a_this%cspline)
    end if
    ! allocate cspline
    allocate( a_this%cspline(nx,3,ny) )
    a_this%cspline = ZERO


    ! loop through all function within the object
    do i=1,size(a_this%y,2)
      call init_cspline(a_this%x,a_this%y(:,i),a_this%cspline(:,:,i))
    end do


    ! successful termination
    if (present(ao_info)) ao_info = .true.
  end subroutine init_cspline_object



  subroutine init_cspline(a_x,a_y,a_cspline)
    ! Initialize cspline array (cspline) for cubic spline interpolation
    ! cspline contains the vectors b,c,d as mentioned below.
    ! The coefficients b(i), c(i), and d(i), i=1,2,...,n are computed
    !  
    ! DESCRIPTION
    ! -----------
    !
    !    s(x) = y(i) + b(i)*(x-x(i)) + c(i)*(x-x(i))**2 + d(i)*(x-x(i))**3
    !
    !    for  x(i) <= x <= x(i+1)
    !
    !  using  '  to denote differentiation,
    !
    !    y(i) = s(x(i))
    !    b(i) = s'(x(i))
    !    c(i) = s''(x(i))/2
    !    d(i) = s'''(x(i))/6 
    !
    ! INPUT
    ! -----
    !  x,y    : arrays of argument and function values 

    ! OUTPUT
    ! ------
    !  cspline : auxiliary cspline fields cspline = ( b(:),c(:),d(:) )
    !
    implicit none

    real(dp), intent(in) :: a_x(:),a_y(:)
    real(dp), target  :: a_cspline(:,:)
    real(dp), pointer :: b(:)=>null()   &
                        ,c(:)=>null()   &
                        ,d(:)=>null()
    real(dp) :: t
    integer :: n,nm1,i,ib

    n = size(a_x)
    if (n /= size(a_y)) stop " error(init_cspline): size of argument&
       & and function field is different"
    
    if ( size(a_cspline,1)<n .or. size(a_cspline,2)<3 )                &
      stop " ERROR(init_cspline): cspline field is too small"

    a_cspline = ZERO
    b => a_cspline(:,1)
    c => a_cspline(:,2)
    d => a_cspline(:,3)
    
    if ( n == 2 ) then
      ! --------------------------------------
      b(1) = (a_y(2)-a_y(1))/(a_x(2)-a_x(1))
      c(1) = 0.
      d(1) = 0.
      b(2) = b(1)
      c(2) = 0.
      d(2) = 0.
    elseif ( n > 2 ) then
      ! --------------------------------------
      nm1 = n-1
      !
      !  set up tridiagonal system
      !
      !  b = diagonal, d = offdiagonal, c = right hand side.
      !
      d(1) = a_x(2) - a_x(1)
      c(2) = (a_y(2) - a_y(1))/d(1)
      do i = 2, nm1
         d(i) = a_x(i+1) - a_x(i)
         b(i) = TWO*(d(i-1) + d(i))
         c(i+1) = (a_y(i+1) - a_y(i))/d(i)
         c(i) = c(i+1) - c(i)
      end do
      !
      !  end conditions.  third derivatives at  x(1)  and  x(n)
      !  obtained from divided differences
      !
      b(1) = -d(1)
      b(n) = -d(n-1)
      c(1) = ZERO
      c(n) = ZERO
      if ( n > 3 ) then
        c(1) = c(3)/(a_x(4)-a_x(2)) - c(2)/(a_x(3)-a_x(1))
        c(n) = c(n-1)/(a_x(n)-a_x(n-2)) - c(n-2)/(a_x(n-1)-a_x(n-3))
        c(1) = c(1)*d(1)**2/(a_x(4)-a_x(1))
        c(n) = -c(n)*d(n-1)**2/(a_x(n)-a_x(n-3))
      end if
      !
      !  forward elimination
      !
      do i = 2, n
         t = d(i-1)/b(i-1)
         b(i) = b(i) - t*d(i-1)
         c(i) = c(i) - t*c(i-1)
      end do
      !
      !  back substitution
      !
      c(n) = c(n)/b(n)
      do ib = 1, nm1
         i = n-ib
         c(i) = (c(i) - d(i)*c(i+1))/b(i)
      end do
      !
      !  c(i) is now the sigma(i) of the text
      !
      !  compute polynomial coefficients
      !
      b(n) = (a_y(n) - a_y(nm1))/d(nm1) + d(nm1)*(c(nm1) + TWO*c(n))
      do i = 1, nm1
         b(i) = (a_y(i+1) - a_y(i))/d(i) - d(i)*(c(i+1) + TWO*c(i))
         d(i) = (c(i+1) - c(i))/d(i)
         c(i) = THREE * c(i)
      end do
      c(n) = THREE * c(n)
      d(n) = d(n-1)
    end if
  end subroutine init_cspline



  pure function diff1_object(a_x0,a_this,ao_fct) Result(res)
    !
    ! user-friendly interface to diff1_cspline.
    ! calculates 1. derivative of functions in cspline object
    !
    !**INPUT/OUTPUT
    !  -------------
    !
    !  a_x0    :  x value to be evaluated
    !
    !  a_this  :  cspline interpolation object containing all the infos
    !
    !  ao_fct  :  function ID in cspline object
    !             default : 1
    !
    implicit none

    type(t_cspline),intent(in) :: a_this
    real(dp), intent(in) :: a_x0
    integer,intent(in),optional :: ao_fct
    real(dp) :: res

    integer :: fct


    res = ZERO

    ! default 1. function
    fct = 1
    if (present(ao_fct)) fct = ao_fct

    ! check consistency
    if ( .not.allocated(a_this%x) .or. .not.allocated(a_this%y) .or.  &
         .not.allocated(a_this%cspline)                                &
       ) then

      return
      !write(STDERR,*) " ERROR(eval_cspline): cspline object is not allocated"
    else
      if ( size(a_this%cspline,3) < fct ) return
      !write(STDERR,*) " ERROR(eval_cspline): number of fct. in object is too small"
    end if

    res = diff1( a_x0,a_this%x,a_this%y(:,fct),a_this%cspline(:,:,fct) )
    
  end function diff1_object


  pure function diff1(a_x0,a_x,a_y,a_cspline) Result(res)
    ! this subroutine calculates the derivative of the cubic spline function
    !
    !    y(x) = y(i) + b(i)*(x-x(i)) + c(i)*(x-x(i))**2 + d(i)*(x-x(i))**3
    !
    !    where  x(i) .lt. u .lt. x(i+1), using horner's rule
    !
    !  if  x0 <  x(1) then  i = 1  is used.
    !  if  x0 >= x(n) then  i = n  is used.
    !
    !  1. derivative : y'(x) = b(i) + 2*c(i)*(x-x(i)) + 3*d(i)*(x-x(i))**2

    !
    ! INPUT
    !   n      : size of the arrays
    !   x0     : argument at which the cspline interpolation should be evaluated
    !   x,y    : arrays of argument and function values
    !   cspline: auxiliary cspline fields
    !            cspline(:,1) = b
    !            cspline(:,2) = c
    !            cspline(:,3) = d
    !
    implicit none

    real(dp),intent(in) ::  a_x0,a_x(:),a_y(:)
    real(dp),intent(in) ::  a_cspline(:,:)
    integer :: i, j, k,sz(2)
    real(dp) :: dx(3),res

    res = ZERO

    sz = shape(a_cspline)
    if (sz(2) /= 3 ) return
    if (sz(1) /= size(a_x) .or. sz(1) /= size(a_y)) return

    if (a_x0 <= a_x(1)) then
      i = 1
    else if (a_x0 >= a_x(sz(1))) then
      i = sz(1)
    else
      ! find segment
      i = 1
      j = sz(1) + 1
      do while (j>i+1)
        k = (i+j)/2
        if ( a_x0 <  a_x(k) ) j = k
        if ( a_x0 >= a_x(k) ) i = k
      end do
    end if

    ! evaluate derivative
    dx(1) = ONE
    dx(2) = TWO*(a_x0 - a_x(i))
    dx(3) = THREE*(dx(2)/TWO)**2

    res = sum(dx*a_cspline(i,:))
  end function diff1



  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Bezier Spline (BSPLINE) !!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  pure function eval_bezier_x(a_x0,a_x,a_y,ao_error) Result(res)
    !  This subroutine evaluates the bezier spline function at the position x0.
    !  The Bezier spline interpolation for z=(x,y) is given by 
    !
    !    z(t) = sum_i=0^N (N over i) t^i (1-t)^(N-i) z_i
    !
    !     if  x0 <  x(1) then  i = 1  is used.
    !     if  x0 >= x(n) then  i = n  is used.
    !
    !  First, the parameter t has to be found
    !
    ! INPUT
    ! -----
    !   x0     : argument at which the spline interpolation should be evaluated
    !   x,y    : arrays of argument and function values 
    !   error  : error in x position
    !
    use basics, only : bino => binominal
    implicit none

    real(dp),intent(in) ::  a_x0,a_x(:),a_y(:),ao_error
    real(dp) :: res,error
    real(dp) :: t,aux,deriv,dx,x,x1,t1
    integer  :: i,n

    optional :: ao_error

    res = ZERO
    n = size(a_y)
    if ( size(a_x)/=n) return
    do i=1,n-1
      if ( a_x(i)>=a_x(i+1) ) return
    end do

    error = 1.0D-6
    if ( present(ao_error) )  error = ao_error

    ! special cases
    if ( a_x0>a_x(n) ) then
      res = a_y(n)
      return
    elseif ( a_x0<a_x(1) ) then
      res = a_y(1)
      return
    end if

    ! find parameter t
    ! -> newton algorithm
    t  = ZERO
    x  = eval_bezier_t(t,a_x)
    t1 = ZERO
    x1 = x
    deriv = a_x(n)-a_x(1)  ! derivative x'(t)
    aux = deriv  ! max. difference in x
    dx = a_x0-x  ! diff. to reference value
    
    do while ( abs(dx)/aux > error )
      x1 = x
      t1 = t

      ! next step
      t = t1 + dx/deriv
      if ( t<ZERO ) t=ZERO
      if ( t>ONE )  t=ONE
      x = eval_bezier_t(t,a_x)

      !new derivative
      dx = a_x0-x
      deriv = (x-x1)/(t-t1)
    end do


    ! get y-value
    res = eval_bezier_t(t,a_y)
  end function eval_bezier_x


  pure function eval_bezier_t(a_t,a_z) Result(res)
    !  This subroutine evaluates the beszier spline function at a given parameter 0<=t<=1
    !  The Bezier spline interpolation for z is given by 
    !
    !    z(t) = sum_i=0^N (N over i) t^i (1-t)^(N-i) z_i
    !
    !  First, the parameter t has to be found
    !
    ! INPUT
    ! -----
    !   t  : curve parameter at which the spline interpolation should be evaluated
    !   z  : reference points 
    !
    use basics, only : bino => binominal
    implicit none
    real(dp), intent(in) :: a_t,a_z(0:)
    
    real(dp) :: w,res
    allocatable :: w(:)
    integer :: i,n

    res = ZERO
    n = size(a_z)-1

    ! check range
    if ( a_t>ONE ) then
      res = a_z(n)
      return
    elseif (a_t<ZERO ) then
      res = a_z(0)
      return
    end if
    if ( all(a_z==ZERO) ) return
    
    ! special cases
    if ( a_t==ONE ) then
      res = a_z(n)
      return
    elseif (a_t==ZERO ) then
      res = a_z(0)
      return
    end if

    ! evaluate the general case
    ! recursive relation (n,k)=(n-k+1)/k*(n,k-1) is used to speed things up
    allocate(w(0:n))
    w(0) = (ONE-a_t)**n
    do i=1,n
      w(i) = (w(i-1)*(n-i+1))/i * a_t / (ONE-a_t)
    end do

    res = sum(w*a_z)
  end function eval_bezier_t

end module  SplineInterpol

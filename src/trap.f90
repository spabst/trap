Program trapro
  
  !
  !   Transformation on a set of datas
  !
  use constants, dp=>dblprec
  use basics, only : countelements,getelements,strlowcase
  use inout, only : flength
  use fft 
  use SplineInterpol, bezier=>eval_bezier_x
  implicit none

  integer :: nstep,nmax
  complex(dp), allocatable,target :: fuc(:,:)
  complex(dp), allocatable :: aux(:)
  complex(dp), pointer :: fct(:)=>null()
  real(dp), pointer ::  x(:)=>null(),dx=>null()
  real(dp),allocatable, target  :: t(:),w(:)
  real(dp),target :: dt,dw

  character(200*clen) :: list,text
  character(clen) :: filein,fileout
  logical :: exist,invert
  integer :: stat
  
  type(t_string), allocatable :: str(:)
  integer,allocatable :: mode(:) ! ways to modifiy the function 
  integer :: nmode ! number of modes
  integer :: icol(2),nfcol  ! column information
  integer :: type ! kind of input function
  integer :: nfile ! number of files in list
  integer :: nfuc  ! number of functions

  integer :: ifile,imode,ifuc

  ! parameters
  real(dp),allocatable :: para(:)
  integer :: npara,ipara
  
  ! auxiliary
  integer  :: i,j,j1,j2
  real(dp) :: dummy1,dummy2

  !   ------------------
  !   |  fft arguments |
  !   ------------------
  !   
  !   program arguments must be stated directly after the executable 
  !
  !   1. argument : CHARACTER - in (input)
  !                 input file name 
  !                 -> contains function that should be Fourier transformed
  !
  !   2. argument : INTEGER - in - optional (type)
  !                 type of function (real/complex)
  !                 default : 1 ( = real)
  !   3. argument : INTEGER - in - optional  (icol)
  !                 columns of function to be read
  !                 = i:j - columns i-th to j-th column are used 
  !                       -> 1. column is reserved for the argument
  !                       -> -1 in i/j means 2./last column 
  !                 = i   <- means i:i 
  !                 default = 2
  !   4. argument : INTEGER - in - optional  (mode)
  !                 defines which action should be performed
  !                 default : 1 (= only FFT)
  !   5. argument : REAL - in - optional 
  !                 needed for action that depend on parameters 
  !                 parameters are deleted after usage such that other
  !                 action can access their parameters

  call getarg(1,list)
  if ( trim(adjustl(list)) == "-v" .or. iargc()<1 ) then
    print *
    print *, " Trafonsformation program (TRAP)"
    print *, " ---------------------------------"
    print *, " written by Stefan Pabst"
    print *, " (c) Stefan Pabst, 2011"
    print *
    call printinfo(6," ")
    if ( iargc()<1 ) write(6,'(a)') ' Too few arguments are given!!!'
    stop
  end if
 
   
  call getarg(1,list)
  open(10,status='scratch')
  inquire(10,name=text)
  list="a=$(ls "//trim(list)//" 2> /dev/null); echo $a > "//trim(text)
  call system(list)
  read(10,'(a)') list
  close(10,status="delete")

  list=adjustl(list)
  if (len_trim(list) == 0 ) call getarg(1,list)

  ! find number of files in list
  nfile = countelements(list," ")
  
  ! optional arguments
  ! ------------------
  ! defualt values
  type=1
  icol=2
  nfuc=1


  ! get 2. argument (real/complex function)
  if ( iargc()>1) then
    call getarg(2,text)
    read(text,*) type
    if ( type<1 .or. type>2 ) stop " ERROR: type is invalid"
    write(STDOUT,'(a,i3)')  " function type ( real[=1] / complex[=2] ) : ", type
  end if


  ! get 3. argument (starting column)
  if (iargc()>2) then
    ! find first and last column
    call getarg(3,text)
    call getelements(str,text,":")
    if ( size(str)>2 )  stop " too many column arguments"
    
    if ( size(str)==1 ) then
      read(str(1)%txt,*)  icol(1)
      icol = icol(1)
    else
      read(str(1)%txt,*)  icol(1)
      read(str(2)%txt,*)  icol(2)
    end if
    if (icol(1)==-1) then
      icol(1)=2
    end if
    if ( any(icol==0) ) icol=0

    ! need even number of columns
    if ( type==3 ) then
      if ( icol(2)>0 .and. mod(icol(2)-icol(1),2)==0 )  icol(2)=icol(2)+1
    end if

    write(STDOUT,'(a,i6,"-",i6)')  " read columns : ", icol
  end if
 

  ! get 3. argument (operations to perform)
  if (iargc()>3) then
    call getarg(4,text)
    call getelements(str,text,":")

    nmode = size(str)
    allocate(mode(nmode))
    mode = 0

    do i=1,nmode
      read(str(i)%txt,*) mode(i)
    end do
    deallocate(str)
  else
    nmode=1
    allocate(mode(nmode))  ! default setting
    mode=1
  end if


  ! 4. argument (optional parameters)
  if (iargc()>4) then
    text=" "
    call getarg(5,text)
    call getelements(str,text,":")

    npara = size(str)
    allocate(para(npara))
    para = ZERO

    do i=1,npara
      call StrLowCase(str(i)%txt)
      
      ! first character "i" means invert number
      invert=.false.
      if ( str(i)%txt(1:1)=="i" ) then
        str(i)%txt = str(i)%txt(2:)
        invert=.true.
      end if

      if ( str(i)%txt=="pi" ) then
        para(i) = PI
      elseif ( str(i)%txt=="e" ) then
        para(i) = exp(ONE)
      else
        read(str(i)%txt,*) para(i)
      end if
      if ( invert )  para(i)=ONE/para(i)
    end do
    deallocate(str)
  else
    npara=0
  end if


  ! --------------------------
  ! | write a trial function |
  ! --------------------------  
  inquire(file=trim(list),exist=exist)
  if ( any(icol==0) .and. nfile<=1 .and. .not.exist) then

     icol=2 ! set to default value
     !print*, 'file "',trim(input),'" does not exist'
     print *, 'create trial function'
     open(55,file=trim(list))
     nmax=2**8-1
     dt=2*pi/nmax
     do i=0,nmax-1
        dummy1=dt*i
        write(55,'(4ES14.6)') dummy1, cos(10.*dummy1)+sin(20.*dummy1)  &
                                      ,5.*sin(5.*dummy1)
     end do
     close(55)
     stop

  elseif (any(icol==0) .and. nfile>1) then

    stop " error: trial function option is not allowed when  &
                  &several input file are given"

  elseif (any(icol==0) .and. exist) then

    stop " error: trial function can't be written in an existing file"

  end if
  

  ! print action to be performed
  write(6,'(a)',advance="no") " action selection : "
  do i=1,nmode
    if ( i==nmode ) then
      write(6,'(i3.2)'  ,advance="no") mode(i)
    else
      write(6,'(i3.2,a)',advance="no") mode(i),","
    end if
  end do
  write(6,'(a)') ""


  write(6,'(a,i3)')        " function is real/complex [1/2] :",type
  write(6,'(a)') ""
  write(6,'(a)',advance="no") " optional parameters : "
  do i=1,npara
    if ( i==npara ) then
      write(6,'(ES13.4)'  ,advance="no") para(i)
    else
      write(6,'(ES13.4,a)',advance="no") para(i),","
    end if
  end do
  write(6,'(a)') ""



  ! --------------------------
  ! --------------------------
  ! | loop through all files |
  ! --------------------------
  ! --------------------------



  ifile=0
  lp_files:do while (len_trim(list)/=0 .and. ifile<nfile)

    filein=list(1:index(list," "))
    list=adjustl(list(index(list," ")+1:)) ! delete current element
    if (len_trim(filein)==0) cycle ! should not happen

    ifile=ifile+1
    write(6,'(/," file (",i3," /",i3," ) = ",a)') ifile,nfile,trim(filein)


    nmax = flength(trim(filein),"E","#")

    if (nmax<=1) stop  ' ERROR: file is too short (just 1 line)'
    write(*,'(a,i10)') ' file length = ', nmax
    nstep = nmax-1
 
    allocate(t(nmax))
    t = ZERO
   


    ! -----------------
    ! | read function |
    ! -----------------
    open(55,file=trim(filein),action="read")

    i=1
    stat = 0
    do while( stat == 0)
      read(55,'(a)',iostat=stat) text
      text=adjustl(text)
      if ( stat /= 0 ) exit
      if (len_trim(text)==0 .or. text(1:1)=="#") cycle


      ! read line
      if ( allocated(fuc) ) then  
        call getelements(str,text," ")
      else
        call getelements(str,text," ")

        ! number of columns in the file
        nfcol = size(str)
        if ( icol(2)==-1 ) icol(2)=nfcol  ! to the last column
        if ( type==2 .and. mod(icol(2)-icol(1),2)==0 )  &
          stop " ERROR: odd number of columns requested (even number is needed for complex functions)"
          
        if ( nfcol<icol(2) )  then
          write(STDERR,*)  " ERROR: number of columns is smaller than requested"
        end if

        nfuc = icol(2)-icol(1)+1
        if ( type==2 )  nfuc=(icol(2)-icol(1)+1)/2

        write(6,'(a,i3,"-",i3)') " read function in columns           :",icol
        write(STDOUT,'(a,i3)')   " number of functions to be processed:",nfuc
        allocate(fuc(nmax,nfuc))
        fuc = ZERO

        if ( allocated(aux)) deallocate(aux)
        allocate(aux(nfuc))
      end if
      aux = ZERO


      ! read data
      read(str(1)%txt,*) t(i)
      do j=1,nfuc

        if ( type==1 ) then
          read(str(icol(1)+j-1)%txt,*) fuc(i,j)
        else
          read(str(icol(1)+2*j-2)%txt,*) fuc(i,j)
          read(str(icol(1)+2*j-1)%txt,*) aux(1)
          fuc(i,j) = fuc(i,j) + IMG*aux(1)
          aux(1) = ZERO
        end if
      end do

      i=i+1
    end do

    ! clean up reading procedure
    close(55)
    if ( allocated(aux) )  deallocate(aux)
    allocate(aux(nmax))
    aux = ZERO

    dt = (t(nmax)-t(1))/nstep  ! more accurrate than t(2)-t(1)



    ! ---------------------------------------
    ! perform operations (order is important)
    ! ---------------------------------------

    write(STDOUT,*)
    lp_fct:do ifuc=1,nfuc
      fct => fuc(:,ifuc)
      write(STDOUT,'(a,i04,a)')  " -> process ",ifuc,". function"
      ipara=1
    
      ! current function argument is treated as time
      ! after a FFT function argument is treated as frequency
      x => t
      dx => dt

      lp_mode:do imode=1,nmode
        select case(mode(imode))
        !-------------------------
        case(43)  ! cube function
          fct = fct**3
 
        !-------------------------
        case(42)  ! square function
          fct = fct**2
        
        !-------------------------
        case(41)  ! square function
          fct = abs(fct)
 
        !-------------------------
        case(39)  ! mulitply x**2
          fct = fct / x**2
 
        !-------------------------
        case(38)  !  multiply x
          fct = fct / x
 
        !-------------------------
        case(32)  ! mulitply x**2
          fct = fct * x**2
 
        !-------------------------
        case(31)  !  multiply x
          fct = fct * x
 
        !-------------------------
        case(30)  !  multiply x
          if ( npara<ipara+1 )  stop " ERROR(action=30): at least two parameters must be provided"

          fct = fct * (para(ipara)+IMG*para(ipara+1))
          !call shorten(npara,para,2)
          ipara = ipara + 2

        !-------------------------
        case(21)  ! integrate
          aux = fct
          fct = ZERO
          
          j1 = 1
          j2 = nmax
          if ( npara>ipara ) then
            ! find boundary points
            j1 = minval( minloc(abs(x-para(ipara))) ) 
            ipara = ipara + 1            
            j2 = minval( minloc(abs(x-para(ipara))) ) 
            ipara = ipara + 1
            write(STDOUT,'(a,2ES13.3)')  "    integration boundaries:",x(j1),x(j2)
          end if
          
          forall (j=j1:j2)
            fct(j) = sum(aux(j1:j)) * dx
          end forall
          fct(j2+1:) = fct(j2)  ! kepp last value till the end
          aux = ZERO
 
        !-------------------------
        case(12)  ! 2nd derivative
          fct = derive2(fct,dx)
 
        !-------------------------
        case(11)  ! 1nd derivative
          fct = derive1(fct,dx)
 
        !-------------------------
        case(1,2,3,4)  ! FFT
 
          ! invert=0/1 : w starts with / centers around 0
          invert = .false.
          if ( mode(imode)==3 .or. mode(imode)==4 ) invert=.true.
          
          ! build omega grid
          if ( associated(x,t) ) then
            if ( allocated(w) ) deallocate(w)
            allocate(w(nmax))
            dw = TWO*pi/(dx*nmax)  ! domega [t(1)=t(nmax+1) => T=2*pi/(dt*nmax), dw=2*pi/T]
            if ( invert ) then
              dummy2 = ((nstep+1)/2) * dw ! center frequency around 0
            else
              dummy2 = ZERO  ! start at w=0
            end if
            forall(i=1:nmax)   w(i) = (i-1)*dw - dummy2
          end if
 
          
          select case(mode(imode))
          case(1,3)
            fct = fft_trafo(fct,1,invert) ! forward Fourier transform
            fct = fct * dx
          case(2,4)
            fct = fft_trafo(fct,2,invert) ! back Fourier transform
            fct = fct * dx
          end select


          ! switch to other argument t->w or w->t
          !  -> in the case w->t, deallocate w
          if ( associated(x,t) ) then
            x  => w
            dx => dw
          elseif ( associated(x,w) ) then
            x  => t
            dx => dt
            deallocate(w)
          else
            stop "ERROR: x has no assignment"
          end if
 
        !-------------------------
        case(51)
          if ( npara<ipara+1 )  stop " ERROR(action=51): at least two parameters must be provided"
          if ( para(ipara+1) == ZERO )  stop " ERROR(action=51): parameter 2 must be non-zero"

          fct = fct * exp( -((x-para(ipara))/para(ipara+1))**2 ) ! * exp(-((x-x0)/a)**2)
          !call shorten(npara,para,2)
          ipara = ipara + 2
 
        !-------------------------
        case(52)
          if ( npara<ipara+1 )  stop " ERROR(action=52): at least two parameters must be provided"
          if ( para(ipara+1) == ZERO )  stop " ERROR(action=52): parameter 2 must be non-zero"

          fct = fct * exp( -(x-para(ipara))/para(ipara+1) )  ! * exp(-(x-x0)/a)
          !call shorten(npara,para,2)
          ipara = ipara + 2

        !-------------------------
        case(60)
          if ( npara<ipara+1 )  stop " ERROR(action=60): at least two parameters must be provided"

          fct = fct + para(ipara) + IMG*para(ipara+1) 
          !call shorten(npara,para,2)
          ipara = ipara + 2

        !-------------------------
        !case(101)  ! bezier interpolation
        !  if ( npara<ipara ) stop " ERROR(action=101): at least one parameter must be provided"
        !  
        !  ! make grid uniform
        !  nullify(x)
        !  dx = ZERO
        !  
        !  ! use arrays "w" and "aux" to store old sampling
        !  if ( allocated(w) ) deallocate(w)
        !  allocate(w(nmax))
        !  w = t
        !  if ( allocated(aux) ) deallocate(aux)
        !  allocate(aux(nmax))
        !  aux = fct
 
        !  ! resize arrays
        !  nmax = int(para(ipara))
        !  nstep=nmax-1
        !  deallocate(t)
        !  allocate(t(0:nstep))
        !  dx=(w(size(w))-w(1))/nstep
        !  forall(i=0:nstep)  t(i) = w(1)+dx*i
        !  x => t
        !  
        !  deallocate(fct)
        !  allocate(fct(0:nstep))
        !  fct = ZERO
 
        !  ! make bezier interpolation
        !  if ( any(aimag(aux)/=ZERO) ) then
        !    forall(j=0:nstep) fct(j) = bezier(t(j),w,dble(aux)) + IMG*bezier(t(j),w,aimag(aux))
        !  else
        !    forall(j=0:nstep) fct(j) = bezier(t(j),w,dble(aux)) 
        !  end if
 
        !  ! deallocate temporary arrays
        !  deallocate(w)
        
        !-------------------------
        case default
          write(*,'(a,i3,a)') " ERROR: action=",mode(imode)," is invalid"
          stop
        end select
      end do lp_mode
    end do lp_fct
  


    ! ------------------------------------------
    ! | write out transformed function | 
    ! ------------------------------------------
    fileout = trim(filein)//'.trap'
    open(56,file=trim(fileout))
    call printinfo(56,"#")
    write(56,'(a)') '#'

    write(56,'(2a)') '# input file: ',trim(filein)
    write(56,'(2a)') '# output file: ',trim(fileout)
    write(56,'(a,i6,"-",i6)')  "# read columns : ", icol
    write(56,'(a,i3)')         "# function is real/complex [1/2] :",type

    write(56,'(a)',advance="no") '# action selection : '
    do j=1,nmode
      if ( j==nmode ) then
        write(56,'(i3.2)'  ,advance="yes") mode(j)
      else
        write(56,'(i3.2,a)',advance="no") mode(j),","
      end if
    end do

    write(56,'(a)',advance="no") '# provided parameters : '
    do j=1,npara
      if ( j==npara ) then
        write(56,'(es12.4)'  ,advance="yes") para(j)
      else
        write(56,'(es12.4,a)',advance="no") para(j),","
      end if
    end do


    write(56,'(a)')
    write(56,'(a)') '#'
    write(56,'(a)') '# x   - argument'
    write(56,'(a)') '# f   - final function'
    write(56,'(a)') '# '
    if ( all(imag(fuc)==ZERO) ) then
      write(56,'(a,i04,a)') '#   x ,  f_i(x)  (i=1...',nfuc,' )'
      do i=1,nmax
         write(56,'(9999es16.6E3)') x(i) , ( real(fuc(i,ifuc)) , ifuc=1,nfuc)
      end do
    else
      write(56,'(a,i04,a)') '#   x ,   real[f_i(x)] ,   imag[f_i(x)] (i=1...',nfuc,' )'
      do i=1,nmax
         write(56,'(9999(2es16.6E3,"  "))') x(i) , ( fuc(i,ifuc) , ifuc=1,nfuc )
      end do
    end if
    close(56)

    nullify(x)
    if (allocated(fuc)) deallocate(fuc)
    if (allocated(aux)) deallocate(aux)
    if (allocated(t)) deallocate(t)
    if (allocated(w)) deallocate(w)

  end do lp_files 

  deallocate(mode)
 
  print *

  ! ###############################
contains      
  ! ###############################			
  
  pure function derive1(fct,dx) Result(fp)
    ! perform 5-point 1. derivative 
    ! Ref.: Book by Muellges-Uhlig 'Numerical Algorithm with Fortran' Chapter 14
    !
    implicit none
    complex(dp),intent(in) :: fct(:)
    real(dp),intent(in) :: dx
    complex(dp),allocatable :: fp(:)
    integer :: i,n


    allocate(fp(size(fct)))
    fp = ZERO

    if ( size(fct) < 5 ) return


    fp(1)=-25*fct(1)+48*fct(2)-36*fct(3)+16*fct(4)-3*fct(5)
    fp(2)=-3*fct(1)-10*fct(2)+18*fct(3)-6*fct(4)+fct(5)


    do i=3,size(fct)-2
       fp(i)=fct(i-2)-8*fct(i-1)+8*fct(i+1)-fct(i+2)
    end do

    n=size(fct)-1 ! second last entry
    fp(n)=3*fct(n+1)+10*fct(n)-18*fct(n-1)+6*fct(n-2)-1*fct(n-3)
    n=size(fct) ! last entry
    fp(n)=25*fct(n)-48*fct(n-1)+36*fct(n-2)-16*fct(n-3)+3*fct(n-4)


    fp=fp/(12.d0*dx)
    return
  end function derive1


  pure function derive2(fct,dx) Result(fpp)
    ! perform 5-point 2. derivative
    ! Ref.: Book by Muellges-Uhlig 'Numerical Algorithm with Fortran' Chapter 14
    !
    implicit none
    complex(dp),intent(in) :: fct(:)
    real(dp),intent(in) :: dx
    complex(dp),allocatable :: fpp(:)
    integer :: i

    
    allocate(fpp(size(fct)))
    fpp = 0.D0

    if ( size(fct) < 5 ) return

    i=1
    fpp(i)=70*fct(i)-208*fct(i+1)+228*fct(i+2)-112*fct(i+3)+22*fct(i+4)
    i=2
    fpp(i)=22*fct(i-1)-40*fct(i)+12*fct(i+1)+8*fct(i+2)-2*fct(i+3)

    do i=3,size(fct)-2
       fpp(i)=-2*fct(i-2)+32*fct(i-1)-60*fct(i)+32*fct(i+1)-2*fct(i+2)
    end do

    i=size(fct)-1 ! second last entry
    fpp(i)=22*fct(i+1)-40*fct(i)+12*fct(i-1)+8*fct(i-2)-2*fct(i-3)

    i=size(fct) ! last entry
    fpp(i)=70*fct(i)-208*fct(i-1)+228*fct(i-2)-112*fct(i-3)+22*fct(i-4)

    fpp=fpp/(24.d0*dx**2)
  end function derive2

  subroutine shorten(a_n,a_para,a_del)
    implicit none
    integer,intent(in) :: a_del
    integer,intent(inout) :: a_n
    real(dp),allocatable :: a_para(:),tmp(:)

    if ( .not.allocated(a_para) ) stop " ERROR(shorten): field is not allocated"
    if ( a_n/=size(a_para) ) stop " ERROR(shorten): size inconsistency"
    if ( a_del > a_n ) stop " ERROR(shorten): too many elements should be deleted"

    if ( a_n==a_del ) then
      a_n=0
      deallocate(a_para)
    else
      a_n = a_n - a_del
      allocate(tmp(a_n))
      tmp = a_para(a_del+1:)
      deallocate(a_para)
      allocate(a_para(a_n))
      a_para = tmp
    end if
  end subroutine


  subroutine printinfo(a_fid,a_c)
    implicit none
    integer,   intent(in) :: a_fid
    character(*), intent(in) :: a_c

    write(a_fid,'(2a)') a_c, 'usage: trap.x filename [type] [column] &
                              &[action][:action][:...] [x0:w]'
    write(a_fid,'(1a)') a_c
    write(a_fid,'(2a)') a_c, 'type = 1  : real function [1 column]'
    write(a_fid,'(2a)') a_c, 'type = 2  : complex fuction [2 columns]'
    write(a_fid,'(1a)') a_c
    write(a_fid,'(2a)') a_c, 'column    : columns used &
                                          &[default=2]'
    write(a_fid,'(2a)') a_c, '     = i:j: use column i to j'
    write(a_fid,'(2a)') a_c, '            -> if only i is provided: j=i'
    write(a_fid,'(2a)') a_c, '            -> i/j=-1 : second/last column'


    write(a_fid,'(1a)') a_c
    write(a_fid,'(2a)') a_c, 'action = 01 : FFT{fct} (forward) [default]'
    write(a_fid,'(2a)') a_c, 'action = 02 : FFT{fct} (backward)'
    write(a_fid,'(2a)') a_c, 'action = 03 : FFT{fct} (forward,  FFT domain centerd at 0) '
    write(a_fid,'(2a)') a_c, 'action = 04 : FFT{fct} (backward, FFT domain centerd at 0)'
    write(a_fid,'(2a)') a_c, 'action = 11 : d/dx fct(x)'
    write(a_fid,'(2a)') a_c, 'action = 12 : d^2/dx^2 fct(x)'
    write(a_fid,'(2a)') a_c, 'action = 21 : Integrate_a^b fct(x) dx, input: a,b'
    write(a_fid,'(2a)') a_c, 'action = 30 :  a  * fct(x), input: Re(a),Im(a)'
    write(a_fid,'(2a)') a_c, 'action = 31 :  x  * fct(x)'
    write(a_fid,'(2a)') a_c, 'action = 32 : x^2 * fct(x)'
    write(a_fid,'(2a)') a_c, 'action = 38 : fct(x)/x'
    write(a_fid,'(2a)') a_c, 'action = 39 : fct(x)/x**2'
    write(a_fid,'(2a)') a_c, 'action = 41 : |fct(x)|'
    write(a_fid,'(2a)') a_c, 'action = 42 : fct(x)**2'
    write(a_fid,'(2a)') a_c, 'action = 43 : fct(x)**3'
    write(a_fid,'(2a)') a_c, 'action = 51 : fct(x)*exp(-(x-x0)**2/w**2), input: x0,w'
    write(a_fid,'(2a)') a_c, 'action = 52 : fct(x)*exp(-(x-x0)/w), input: x0,w'
    write(a_fid,'(2a)') a_c, 'action = 60 :  a + fct(x), input: Re(a),Im(a)'
    !write(a_fid,'(2a)') a_c, 'action = 101: bezier interpolation with N uniform grid points, input: N'

    write(a_fid,'(1a)') a_c
  end subroutine printinfo

  
end Program trapro

module fft
  !
  !**PURPOSE
  !  -------
  !  Perfom foward and backward FFT (non-symmetric version)
  !  during the back-transformation 1/2/pi gets multiplied
  !
  !   f(w) = \int f(t) e^-iwt dt
  !
  !   f(t) = 1/2/pi \int f(w) e^iwt dw
  !
  !**Description
  !  -----------
  !  This module contains the following functions/subroutines:
  !
  !    -> (fct) fft_trafo : forward [exp(-iwt)] & backward [exp(+iwt)]]
  !
  use constants, dp=>dblprec
  implicit none


contains

  function fft_trafo(a_fct,a_mode,ao_switch) Result(a_fft)
    !
    !**INPUT/OUTPUT
    !  
    !  a_mode : = 1 - forward FFT  (non-symmetric)
    !           = 2 - backward FFT (factor 1/2/pi)
    !  ao_switch : = .false - FFT domain is not switched (starts with 0)
    !                         [default]
    !              = .true. - FFT domain is switched (centered around 0)
    implicit none

    integer, intent(in)     :: a_mode
    logical, intent(in), optional :: ao_switch

    logical :: switch
    complex(dp), intent(in) :: a_fct(0:)
    complex(dp),allocatable :: a_fft(:) !size(a_fct)-1)
    
    integer :: i,n
    complex(dp),allocatable :: c(:),aux(:)
    real(dp),allocatable :: aux1(:),aux2(:),aux3(:)
    

    switch=.false.
    if (present(ao_switch))  switch = ao_switch

    n = size(a_fct)
    allocate(a_fft(0:n-1))
    a_fft = ZERO

    if ( a_mode /= 1 .and. a_mode/=2 ) stop &
      " ERROR(FFT): wrong mode (=1 forward FFT;=2 backward FFT)"

    ! only one data point
    if ( n==1 ) return


    ! setup array
    allocate(aux1(2*n),aux2(2*n))
    allocate(aux3(15))
    aux1 = ZERO
    aux2 = ZERO
    aux3 = ZERO

    ! needed to change frequency domain
    allocate(aux(0:n-1))
    aux = ZERO
 

    a_fft = a_fct

    ! initialize
    call cffti1(n,aux2,aux3)

    ! perform trafo
    select case(a_mode)
    case(1)
      
      call cfftf1(n,a_fft,aux1,aux2,aux3)

      !! change frequency domain such that it is centered around 0
      if (switch) then
        do i=0,n-1
          aux(modulo(n/2+i,n))=a_fft(i)
        end do
        a_fft = aux
      end if

    case(2)

      !! change frequency domain such that 0 at the bourder
      if ( switch) then
        do i=0,n-1
          aux(modulo((n+1)/2+i,n))=a_fft(i)
        end do
        a_fft = aux
      end if

      call cfftb1(n,a_fft,aux1,aux2,aux3)
    case default
      stop " ERROR: invalid mode"
    end select
   

    deallocate(aux,aux1,aux2,aux3)
  end function fft_trafo

end module fft

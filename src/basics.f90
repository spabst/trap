module basics
  ! -------------------------------------------------------------------------
  !**PURPOSE
  !  -------
  !  The module BASICS contains general function and subroutines that are
  !  widely-useable.
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of this 
  !  module:
  !
  ! MATHEMATICAL OPERATIONS / FUNCTIONS
  ! -----------------------------------
  !  -> (fct) eulerrotation   : vector expressed in the coordinate of the 
  !                             rotated coordinate system defined by Euler 
  !                             anlges  a,b,c [use "y-convention"]
  !  -> (fct) inveulerrotation: inverse rotation (a,b,c) -> (-c,-b,a)
  !
  !  -> (fct) cross_product  : vector cross prodcut of a 3D vector
  !
  !  -> (fct) derivative1    : 1. derivative of real function (5-point rule)
  !
  !  -> (fct) derivative2    : 2. derivative of real function (5-point rule)
  !
  !  -> (fct) errorFct       : error function for complex arguments
  !                            rewritten from Prof. Jianming Jin's (UIUC) 
  !                            FORTRAN-77 code
  !
  !  -> (fct) binominal      : calculate binominal n!/(k!*(n-k)!)
  !

  !
  !
  ! STRING OPERATIONS
  ! -----------------
  !  -> (sbr) atomno         : find atom number from atom symbol or vice versa
  !                            
  !  -> (fct) numberformat   : determine number format for write()
  !
  !  -> (fct) countsymbol(str,c) 
  !                          : number of times the symbols in c occur in str
  !  -> (fct) countelements(str,c) 
  !                          : number of elements in the line
  !  -> (sbr) getelements(str,c,string)
  !                          : store elements (separated by c) in string
  !
  !  -> (sbr) StrUpCase      : change all lower case character into upper case
  !  -> (sbr) StrLowCase    : change all upper case character into lower case
  !
  !
  ! TIME and DATE
  ! --------------
  !  -> (fct) timediff(a[,b]): get time difference between timestemp a and
  !                            timestemp b or now
  !  -> (fct) daydiff        : number of days between two dates
  !  -> (sbr) timeinfo       : Print informations about time points passed
  !                            into the routine.
  !
  !**AUTHOR INFORMATION
  !  ------------------
  !  written by Stefan Pabst in June 2011
  !
  !**VERSION INFO
  !  ------------
  !  $Id: basics.f90 684 2012-12-24 13:17:01Z spabst $
  !-------------------------------------------------------------------------
  !
  use constants , dp=>dblprec
  implicit none


  interface derivative1
    module procedure  derivative1_real,derivative1_complex
  end interface derivative1
  
  interface derivative2
    module procedure  derivative2_real,derivative2_complex
  end interface derivative2
  

contains


  pure function rotation_passive(vec,a,b,c) Result(euler)
    !
    ! Evaluate the vector in the new the rotated coordinate system 
    ! (passive rotation).
    ! a,b,c are the Euler angle defining the rotation of the coord. system.
    ! Here the "x-convention" is used (see wikipedia for the active rotation), 
    ! meaning the rotations are made
    ! firstly around the z axes (a angle), then around the x axis (b angle), 
    ! and lastly around the new z' axis (c angle).
    !
    !   => R_z(-c) R_x(-b) R_z(-a) = R
    !
    ! Due to numerical efficiency the rotation matrix R_ij is stored in RT_ji
    ! s.t the j index is the first index and i the last.
    ! Finally the matrix-vector product  
    !
    !   new_i = sum_j RT_ji old_j
    !
    ! will be calculated.
    !
    implicit none
    real(dp),intent(in) :: vec(3), a,b,c
    real(dp) :: RT(3,3),euler(3)
    real(dp) :: cos_a,sin_a,cos_b,sin_b,cos_c,sin_c
    integer :: i
    
    sin_a = sin(a)
    cos_a = cos(a)
    sin_b = sin(b)
    cos_b = cos(b)
    sin_c = sin(c)
    cos_c = cos(c)

    RT(1,1) = cos_a*cos_c - sin_a*cos_b*sin_c 
    RT(2,1) = sin_a*cos_c + cos_a*cos_b*sin_c
    RT(3,1) = sin_b*sin_c
          
    RT(1,2) =-cos_a*sin_c - sin_a*cos_b*cos_c
    RT(2,2) =-sin_a*sin_c + cos_a*cos_b*cos_c 
    RT(3,2) = sin_b*cos_c
          
    RT(1,3) = sin_a*sin_b
    RT(2,3) =-cos_a*sin_b
    RT(3,3) = cos_b

   
    forall(i=1:3)
     euler(i) = sum( RT(1:3,i)*vec )
    end forall
  end function rotation_passive

  
  pure function rotation_active(vec,a,b,c) Result(euler)
    !
    ! active rotation in the "x"-convention: 
    !  -> active: rotate vector (same coordinate system)
    !  -> rotate around 1. z (a angle), 
    !                   2. x (b angle),
    !                   3. z (c angle)
    !
    !   => v_new = R_z(a) R_x(b) R_z(c) v_old
    !            = R v_old
    !
    ! Due to numerical efficiency the rotation matrix R_ij is stored in RT_ji
    ! s.t the j index is the first index and i the last.
    ! Finally the matrix-vector product  
    !
    !   new_i = sum_j RT_ji old_j
    !
    ! will be calculated.
    !
    implicit none
    real(dp),intent(in) :: vec(3), a,b,c
    real(dp) :: RT(3,3),euler(3)
    real(dp) :: cos_a,sin_a,cos_b,sin_b,cos_c,sin_c
    integer :: i
    
    sin_a = sin(a)
    cos_a = cos(a)
    sin_b = sin(b)
    cos_b = cos(b)
    sin_c = sin(c)
    cos_c = cos(c)

    RT(1,1) = cos_a*cos_c - sin_a*cos_b*sin_c 
    RT(2,1) =-cos_a*sin_c - sin_a*cos_b*cos_c
    RT(3,1) = sin_a*sin_b
          
    RT(1,2) = sin_a*cos_c + cos_a*cos_b*sin_c
    RT(2,2) =-sin_a*sin_c + cos_a*cos_b*cos_c 
    RT(3,2) =-cos_a*sin_b
          
    RT(1,3) = sin_b*sin_c
    RT(2,3) = sin_b*cos_c
    RT(3,3) = cos_b

    forall(i=1:3)
     euler(i) = sum( RT(1:3,i)*vec )
    end forall
  end function rotation_active

  
  pure function cross_product(a_v1,a_v2) Result(v)
    ! do 3D vector cross product
    real(dp),intent(in) :: a_v1(3),a_v2(3)
    real(dp) :: v(3)

    v(1) = a_v1(2)*a_v2(3) - a_v1(3)*a_v2(2)
    v(2) = a_v1(3)*a_v2(1) - a_v1(1)*a_v2(3)
    v(3) = a_v1(1)*a_v2(2) - a_v1(2)*a_v2(1)
  end function


  ! ----------------------------------------


  pure function derivative1_real(fct,dx) Result(fp)
    ! perform 5-point 1. derivative 
    ! Ref.: Book by Muellges-Uhlig 'Numerical Algorithm with Fortran' Chapter 14
    !
    real(dp),intent(in) :: fct(:)
    real(dp),intent(in) :: dx
    real(dp) :: fp(size(fct))
    integer :: i

    if ( size(fct) < 5 ) then
      fp = ZERO
      return
    end if

    fp(1)=-25*fct(1)+48*fct(2)-36*fct(3)+16*fct(4)-3*fct(5)
    fp(2)=-3*fct(1)-10*fct(2)+18*fct(3)-6*fct(4)+fct(5)

    do i=3,size(fct)-2
      fp(i)=fct(i-2)-8*fct(i-1)+8*fct(i+1)-fct(i+2)
    end do

    i=size(fct)-1 ! second last entry
    fp(i)=3*fct(i+1)+10*fct(i)-18*fct(i-1)+6*fct(i-2)-1*fct(i-3)
    i=size(fct) ! last entry
    fp(i)=25*fct(i)-48*fct(i-1)+36*fct(i-2)-16*fct(i-3)+3*fct(i-4)

    fp=fp/(12.d0*dx)
  end function derivative1_real


  pure function derivative2_real(fct,dx) Result(fpp)
    ! perform 5-point 2. derivative
    ! Ref.: Book by Muellges-Uhlig 'Numerical Algorithm with Fortran' Chapter 14
    !
    real(dp),intent(in) :: fct(:)
    real(dp),intent(in) :: dx
    real(dp) :: fpp(size(fct))
    integer :: i

    fpp = 0.D0
    if ( size(fct) < 5 ) return

    i=1
    fpp(i)=70*fct(i)-208*fct(i+1)+228*fct(i+2)-112*fct(i+3)+22*fct(i+4)
    i=2
    fpp(i)=22*fct(i-1)-40*fct(i)+12*fct(i+1)+8*fct(i+2)-2*fct(i+3)

    do i=3,size(fct)-2
      fpp(i)=-2*fct(i-2)+32*fct(i-1)-60*fct(i)+32*fct(i+1)-2*fct(i+2)
    end do

    i=size(fct)-1 ! second last entry
    fpp(i)=22*fct(i+1)-40*fct(i)+12*fct(i-1)+8*fct(i-2)-2*fct(i-3)

    i=size(fct) ! last entry
    fpp(i)=70*fct(i)-208*fct(i-1)+228*fct(i-2)-112*fct(i-3)+22*fct(i-4)

    fpp=fpp/(24.d0*dx**2)
  end function derivative2_real


  pure function derivative1_complex(fct,dx) Result(fp)
    ! perform 5-point 1. derivative 
    ! Ref.: Book by Muellges-Uhlig 'Numerical Algorithm with Fortran' Chapter 14
    !
    complex(dp),intent(in) :: fct(:)
    real(dp),intent(in) :: dx
    complex(dp) :: fp(size(fct))
    integer :: i

    if ( size(fct) < 5 ) then
      fp = 0.0
      return
    end if

    fp(1)=-25*fct(1)+48*fct(2)-36*fct(3)+16*fct(4)-3*fct(5)
    fp(2)=-3*fct(1)-10*fct(2)+18*fct(3)-6*fct(4)+fct(5)

    do i=3,size(fct)-2
       fp(i)=fct(i-2)-8*fct(i-1)+8*fct(i+1)-fct(i+2)
    end do

    i=size(fct)-1 ! second last entry
    fp(i)=3*fct(i+1)+10*fct(i)-18*fct(i-1)+6*fct(i-2)-1*fct(i-3)
    i=size(fct) ! last entry
    fp(i)=25*fct(i)-48*fct(i-1)+36*fct(i-2)-16*fct(i-3)+3*fct(i-4)

    fp=fp/(12.d0*dx)
  end function derivative1_complex


  pure function derivative2_complex(fct,dx) Result(fpp)
    ! perform 5-point 2. derivative
    ! Ref.: Book by Muellges-Uhlig 'Numerical Algorithm with Fortran' Chapter 14
    !
    complex(dp),intent(in) :: fct(:)
    real(dp),intent(in) :: dx
    complex(dp) :: fpp(size(fct))
    integer :: i

    fpp = 0.D0
    if ( size(fct) < 5 ) return

    i=1
    fpp(i)=70*fct(i)-208*fct(i+1)+228*fct(i+2)-112*fct(i+3)+22*fct(i+4)
    i=2
    fpp(i)=22*fct(i-1)-40*fct(i)+12*fct(i+1)+8*fct(i+2)-2*fct(i+3)

    do i=3,size(fct)-2
       fpp(i)=-2*fct(i-2)+32*fct(i-1)-60*fct(i)+32*fct(i+1)-2*fct(i+2)
    end do

    i=size(fct)-1 ! second last entry
    fpp(i)=22*fct(i+1)-40*fct(i)+12*fct(i-1)+8*fct(i-2)-2*fct(i-3)

    i=size(fct) ! last entry
    fpp(i)=70*fct(i)-208*fct(i-1)+228*fct(i-2)-112*fct(i-3)+22*fct(i-4)

    fpp=fpp/(24.d0*dx**2)
  end function derivative2_complex


  ! ----------------------------------------

  pure function errorFct(z) result(cer)
    !
    !**PURPOSE
    !  -------
    !  error function for complex arguments
    !   Err(x) = 2/sqrt(pi) * int_0^x exp(-t**2) dt
    !
    !**INPUT/OUTPUT
    !  ------------
    !   z  : COMPLEX - IN
    !        complex argument
    !  cer : COMPLEX - OUT
    !         = err(z)
    implicit none

    complex(dp), intent(in) :: z
    complex(dp) :: cer

    complex(dp) :: c0,cs,cr,cl,z1
    real(dp)    :: a0
    integer     :: k


    A0=CDABS(Z)
    C0=CDEXP(-Z*Z)
    Z1=Z

    IF ( REAL(Z).LT.ZERO ) THEN
       Z1=-Z
    ENDIF

    IF ( A0.LE.5.8D0 ) THEN    
      CS=Z1
      CR=Z1
      DO K=1,120
        CR=CR*Z1*Z1/(K+0.5D0)
        CS=CS+CR
        IF ( CDABS(CR/CS).LT.1.0D-15 ) exit
      END DO
      CER=2.0D0*C0*CS/DSQRT(PI)
    ELSE                              
      CL=1.0D0/Z1              
      CR=CL
      DO K=1,13
        CR=-CR*(K-0.5D0)/(Z1*Z1)
        CL=CL+CR
        IF (CDABS(CR/CL).LT.1.0D-15) exit
      END DO
      CER=1.0D0-C0*CL/DSQRT(PI)
    ENDIF

    IF ( REAL(Z).LT.0.0 ) CER=-CER
  end function errorFct


  ! ----------------------------------------

  pure function binominal(a_n,a_k) result(res)
    !
    !**PURPOSE
    !  -------
    !  calculate binominal coefficient
    !
    !    ( n )          n!
    !    (   )  = ---------------    n>k
    !    ( k )      k!  (n-k)!
    !
    !  To do so, the logarithm will be used to avoid
    !  too large numbers
    !
    !**INPUT/OUTPUT
    !  ------------
    !   a_n   : INTEGER - IN
    !   a_k   : INTEGER - IN
    !   a_res : Real - OUT 
    !           number can be easily much larger than integer space
    !
    implicit none

    integer, intent(in) :: a_n,a_k
    real(dp) :: res

    real(dp),allocatable :: aux1(:),aux2(:)
    real(dp) :: aux
    integer :: i,j,ks,kl

    res = ZERO
    ! test range
    if ( a_k > a_n ) return
    if ( a_k < 0 ) return

    ! special cases
    if ( a_k==a_n .or. a_k==0 ) then
      res = ONE
      return
    elseif ( a_k==1 .or. a_k==a_n-1) then
      res = ONE*a_n
      return
    end if
   
    ks = min(a_k,a_n-a_k)
    kl = max(a_k,a_n-a_k)

    allocate( aux1(ks) , aux2(ks) )
    forall(i=1:ks) aux1(i) = a_n+1-i
    forall(i=1:ks) aux2(i) = ks+1-i

    do i=1,ks ;  do j=1,ks
      if ( aux2(j)==1 ) then
        cycle
      else if ( mod(aux1(i),aux2(j))==0 ) then
        aux1(i)=aux1(i)/aux2(j)
        aux2(j)=1
      end if
    end do; end do
    
    res = product(aux1*ONE/aux2)
  end function binominal




  ! ----------------------------------------
  !         -- STRING OPERATIONS --
  ! ----------------------------------------



  subroutine atomno(symbol,z,mode)
    ! return atom number of a atom symbol (mode="Z" - default)
    ! or vice versa (mode="S")
    ! Atom names (www.webelements.com - 31.12.2010)
    ! e.g. H -> 1 ; He -> 2; Xe -> 54
    !
    implicit none

    character(LEN=3),intent(inout) :: symbol
    character,intent(in),optional :: mode
    integer,intent(inout) :: z

    character :: omode
    character(LEN=3) :: aux
    type(t_string) :: element(118)    
    
    integer :: i

    omode="Z"
    if (present(mode)) omode=mode
    call strUpCase(omode)
    if ( omode/="S".and.omode/="Z") stop " ERROR(atomno): mode is invalid"
    
    aux=symbol
    call StrUpCase(aux)

    ! initiate element field
    element(1)%txt='H'
    element(2)%txt='HE'
    element(3)%txt='LI'
    element(4)%txt='BE'
    element(5)%txt='B'
    element(6)%txt='C'
    element(7)%txt='N'
    element(8)%txt='O'
    element(9)%txt='F'
    element(10)%txt='NE'
    element(11)%txt='NA'
    element(12)%txt='MG'
    element(13)%txt='AL'
    element(14)%txt='SI'
    element(15)%txt='P'
    element(16)%txt='S'
    element(17)%txt='CL'
    element(18)%txt='AR'
    element(19)%txt='K'
    element(20)%txt='CA'
    element(21)%txt='SC'
    element(22)%txt='TI'
    element(23)%txt='V'
    element(24)%txt='CR'
    element(25)%txt='MN'
    element(26)%txt='FE'
    element(27)%txt='CO'
    element(28)%txt='NI'
    element(29)%txt='CU'
    element(30)%txt='ZN'
    element(31)%txt='GA'
    element(32)%txt='GE'
    element(33)%txt='AS'
    element(34)%txt='SE'
    element(35)%txt='BR'
    element(36)%txt='KR'
    element(37)%txt='RB'
    element(38)%txt='SR'
    element(39)%txt='Y'
    element(40)%txt='ZR'
    element(31)%txt='NB'
    element(42)%txt='MO'
    element(43)%txt='TC'
    element(44)%txt='RU'
    element(45)%txt='RH'
    element(46)%txt='PD'
    element(47)%txt='AG'
    element(48)%txt='CD'
    element(49)%txt='IN'
    element(50)%txt='SN'
    element(51)%txt='SB'
    element(52)%txt='TE'
    element(53)%txt='I'
    element(54)%txt='XE'
    element(55)%txt='CS'
    element(56)%txt='BA'
    element(57)%txt='LA'
    element(58)%txt='CE'
    element(59)%txt='PR'
    element(60)%txt='ND'
    element(61)%txt='PM'
    element(62)%txt='SM'
    element(63)%txt='EU'
    element(64)%txt='GD'
    element(65)%txt='TB'
    element(66)%txt='DY'
    element(67)%txt='HO'
    element(68)%txt='ER'
    element(69)%txt='TM'
    element(70)%txt='YB'
    element(71)%txt='LU'
    element(72)%txt='HF'
    element(73)%txt='TA'
    element(74)%txt='W'
    element(75)%txt='RE'
    element(76)%txt='OS'
    element(77)%txt='IR'
    element(78)%txt='PT'
    element(79)%txt='AU'
    element(80)%txt='HG'
    element(81)%txt='TL'
    element(82)%txt='PB'
    element(83)%txt='BI'
    element(84)%txt='PO'
    element(85)%txt='AT'
    element(86)%txt='RN'
    element(87)%txt='FR'
    element(88)%txt='RA'
    element(89)%txt='AC'
    element(90)%txt='TH'
    element(91)%txt='PA'
    element(92)%txt='U'
    element(93)%txt='NP'
    element(94)%txt='PU'
    element(95)%txt='AM'
    element(96)%txt='CM'
    element(97)%txt='BK'
    element(98)%txt='CF'
    element(99)%txt='ES'
    element(100)%txt='FM'
    element(101)%txt='MD'
    element(102)%txt='NO'
    element(103)%txt='LR'
    element(104)%txt='RF'
    element(105)%txt='DB'
    element(106)%txt='SG'
    element(107)%txt='BH'
    element(108)%txt='HS'
    element(109)%txt='MT'
    element(110)%txt='DS'
    element(111)%txt='RG'
    element(112)%txt='CN'
    element(113)%txt='UUT'
    element(114)%txt='UUQ'
    element(115)%txt='UUP'
    element(116)%txt='UUH'
    element(117)%txt='UUS'
    element(118)%txt='UUO'

    ! select mode

    select case(mode)
    case("S")  ! z-> symbol

      if (z<1 .or. z>118) stop "error (atomno): Z value is out of range"
      symbol = element(z)%txt

    case("Z")  ! symbol -> Z

      do i=1,118
        if (trim(element(i)%txt) == trim(symbol)) then
          z=i
          exit
        end if
      end do

    case default

      stop "error (atomno): mode is unknown!!!"

    end select

  end subroutine atomno


  function numberformat ( a_mode , a_num , ao_space , ao_rep , ao_small , ao_zero , ao_expo )   Result(form)
    !
    !**PURPOSE
    !  Determine format of number in procedure write() and/or read()
    !**DESCRIPTION
    !  Standard formats for integer  and float number are
    !  "i5.3" and "ES13.4E3", respectively.
    !  This routine finds the number of characters needed to write the integer/float number.
    !**INPUT/OUTPUT
    !  form    : CHARACTER(clen) - OUT
    !            string containing the format needed  to print the number.
    !
    !  a_mode  : CHARACTER(+) - IN
    !            Defines the output style
    !             = "I" - integer
    !             = "F" - float (fix point)
    !             = "ES"- scientific exponent
    !
    !  a_num   : REAL(DP) - IN
    !            Example of the number that should be written/read
    !              a_mode="I" : a number of similar length
    !              a_mode="F" : largest possible number that can be illustrated
    !                           number of decimal places
    !                           -> if 0 write number like an integer
    !              a_mode="ES": ratio between largest and smallest number
    !                           at least one decimal place
    !
    !  ao_space: INTEGER - IN - OPTIONAL
    !            number of blank characters in front of number
    !            default = 1
    !
    !  ao_rep  : INTEGER - IN - OPTIONAL
    !            number of repetitions
    !            default = 1
    !
    !  ao_small: INTEGER - IN - OPTIONAL
    !            smallest possible number that can be illustrated
    !            Only applies for a_mode="F"
    !            default = 0.1
    !
    !  ao_zero : LOGICAL - IN - OPTIONAL
    !            Fixed length of number (if necessary use 0 to fill the length)
    !            Only applies for a_mode="I"
    !            default = false
    !
    !  ao_expo : INTEGER - IN - OPTIONAL
    !            Length of the exponent statement
    !            Only applies for a_mode="ES"
    !            default = 2
    !
    implicit none

    real(dp), intent(in) :: a_num
    character(*), intent(in) :: a_mode
    integer, intent(in), optional :: ao_space,ao_expo,ao_rep
    logical, intent(in), optional :: ao_zero
    real(dp), intent(in), optional :: ao_small
    character(clen) :: form

    integer :: space,expo,l,z,rep
    logical :: zero
    real(dp) :: small
    character(2) :: f1,f2,f3


    ! initialize return value
    form = ""

    ! check optional arguments
    space = 1
    if ( present(ao_space) ) space = ao_space
    ! space = -1 : works only for positive numbers
    if ( space < -1 ) stop " ERROR(numberformat): no negative space"

    zero = .false.
    if ( present(ao_zero) ) zero = ao_zero

    expo=2
    if ( present(ao_expo) ) expo = ao_expo
    if ( expo < 1 ) stop " ERROR(numberformat): positive exponent width required"
    write(f3,'(i1)') expo

    rep=1
    if ( present(ao_rep) ) rep = ao_rep
    if ( rep < 1 ) stop " ERROR(numberformat): positive repetition rate required"

    small = 0.1
    if ( present(ao_small) ) small = ao_small
    if ( small == 0 ) stop " ERROR(numberformat): smallest visible number must be nonzero"


    ! check number format
    select case ( trim(adjustl(a_mode)) )

    case("es","ES")
      ! ratio of highest and smallest number
      z = max( 1 , ceiling( log10(abs(a_num)) ) )
      write(f2,'(i2)') z
      ! +5 : minus sign, leading digit, "." sign; and "E+" or "E-" for exponent
      l = z + 5 + space + expo
      write(f1,'(i2)') l

      form = trim(adjustl(a_mode)) // trim(adjustl(f1)) // "." &
            // trim(adjustl(f2)) // "E" // trim(adjustl(f3))

    case("F","f")
      ! smallest number representable
      z = max( 0 , -floor( log10(abs(small)) ) )
      write(f2,'(i2)') z

      ! +2 : minus sign, and "." sign
      ! +leading digits
      l = z + 2 + space + max(1,ceiling(log10(abs(a_num))))
      write(f1,'(i2)') l

      form = trim(adjustl(a_mode)) // trim(adjustl(f1)) // "." &
            // trim(adjustl(f2))

    case("i","I")
      ! ratio of highest and smallest number
      z = floor( log10(abs(a_num)) ) + 1
      write(f2,'(i2)') z

      ! +1 : minus sign
      l = z + 1 + space
      write(f1,'(i2)') l

      form = trim(adjustl(a_mode)) // trim(adjustl(f1))
      if ( zero ) then
        form = trim(form) // "." // trim(adjustl(f2))
      end if

    end select


    ! repition
    if ( rep > 1 ) then
      write(f1,'(i2)') rep
      form = trim(adjustl(f1)) // trim(form)
    end if

  end function numberformat


  ! ---------------------------------------------------------
  ! ---------------------------------------------------------


  pure integer function countsymbol(a_str,a_c) Result(num)
    !
    ! count the number of times any character sequence in  "c" appears in "str"
    !
    ! INPUT
    ! ------
    !  a_str : string to be searched
    !  a_c   : character to be searched for
    !
    implicit none
    character(*),intent(in) :: a_str
    character(*),intent(in) :: a_c
    integer :: pos,apos,strlen

    num = 0
    pos = 0
    apos = 1
    strlen = len(a_str)

    ! start counting
    do
      pos = scan( a_str(apos:) , a_c )
      apos = apos + pos 
      if (pos==0 .or. apos>strlen) exit

      num = num + 1
    end do

  end function countsymbol

  ! -----------------------------------------------------------------


  pure integer function countelements(a_str,a_delim) Result(num)
    !
    ! Count number of elements in a string. Elements are separated by
    ! the deliminator. Empty elements do not count.
    !
    ! INPUT
    ! -----
    !  a_str   : CHARACTER - in
    !            string to be searched
    !  a_delim : CHARACTER - in - optional
    !            series of symbols defining the separator between the elements
    !
    ! OUTPUT
    ! ------
    !  num     : INTEGER - out
    !            number of elements found in a_str
    !
    implicit none
    character(*),intent(in) :: a_str,a_delim
    integer :: pos,next,pend

    num = 0
    pos = 0
    next = 1
    ! go through string
    do while ( next <= len(a_str) )
      pos=next
      next=index(a_str(pos:),a_delim)

      ! no more separators
      if ( next==0 ) then
        if ( len_trim(adjustl(a_str(pos:)))/=0 ) num = num + 1
        exit
      end if

      ! go to the end of the element
      pend = pos -2 + next
      ! go behind next separator
      next = pend + 1 + len(a_delim)

      ! if length of element is 0 don't count
      if (pend<pos) cycle
      if ( len_trim(adjustl(a_str(pos:pend))) /= 0 ) num = num +1
    end do
  end function countelements



  subroutine getelements(a_ele,a_str,a_delim)
    !
    ! Find all elements separated by a_ele and return them in an own-defined
    ! string array.
    !
    ! INPUT
    ! -----
    !  a_str   : CHARACTER - in
    !            string to be searched
    !  a_delim : CHARACTER - in - optional
    !            series of symbols defining the separator between the elements
    !
    ! OUTPUT
    ! ------
    !  a_ele   : STRING,ALLOCATABLE - out
    !            each element is stored in a separate character field
    !
    implicit none
    character(*), intent(in) :: a_str,a_delim
    type(t_string), intent(inout),allocatable :: a_ele(:)

    integer :: i,n,ntot
    integer :: pbegin,pend,pnext

    if (allocated(a_ele)) deallocate(a_ele)
   
    ! number of non-empty elements
    ntot = countelements(a_str,a_delim)
    if ( ntot==0 ) return  ! no elementy -> do nothing

    allocate(a_ele(ntot))

    n = 0
    pbegin=0
    pend=0
    pnext=1
    ! go through string
    do while ( pnext <= len(a_str) .and. n<ntot )
      pbegin=pnext
      pnext=index(a_str(pbegin:),a_delim)

      ! no more separators
      if (pnext==0) then

        if ( len_trim(adjustl(a_str(pbegin:)))/=0) then
          n = n + 1
          read(a_str(pbegin:),'(a)') a_ele(n)%txt
          a_ele(n)%txt = adjustl(a_ele(n)%txt)          
        end if

        exit
      end if

      ! go at the end of the current element
      pend  = pbegin + pnext -2
      ! go behind next separator
      pnext = pbegin + pnext -1 + len(a_delim)

      ! if length of element is 0 skip
      if (pend<pbegin) cycle
      if ( len_trim(adjustl(a_str(pbegin:pend))) == 0 ) cycle

      ! store non-empty element
      n = n +1
      read(a_str(pbegin:pend),'(a)') a_ele(n)%txt
      a_ele(n)%txt = adjustl(a_ele(n)%txt)
    end do

  end subroutine getelements

  ! -----------------------------------------------------------------


  pure subroutine StrUpCase(string)
    ! Case Converter
    ! subroutine to convert all lower case to upper case
    ! string: input and output string

    implicit none
    character(*),intent(inout) :: string
    integer :: jgap,i

    jgap=ICHAR('A')-ICHAR('a')
    ! this could be replaced by jgap=-32, since the ASCII code for "a" is
    ! 97 and for "A" is 65, using the ICHAR function ensures robustness
    ! in case other systems are used (like EBCDIC--hey I'm an old timer)

    do i=1,len_trim(string)
       if (string(i:i).LE.'z' .and. string(i:i).GE.'a') &
            string(i:i)=CHAR(ICHAR(string(i:i))+jgap)
    end do
    return

  end Subroutine StrUpCase


  pure subroutine StrLowCase(string)
    ! Case Converter
    ! subroutine to convert all upper case to lower case
    ! string: input and output string

    implicit none
    character(*),intent(inout) :: string
    integer :: jgap,i

    jgap=ICHAR('a')-ICHAR('A')
    ! this could be replaced by jgap=32, since the ASCII code for "a" is
    ! 97 and for "A" is 65, using the ICHAR function ensures robustness
    ! in case other systems are used (like EBCDIC--hey I'm an old timer)

    do i=1,len_trim(string)
       if (string(i:i).LE.'Z' .and. string(i:i).GE.'A') &
            string(i:i)=CHAR(ICHAR(string(i:i))+jgap)
    end do
    return

  end Subroutine StrLowCase


  ! -----------------------------------------------------------------
  !      Time-related Routines
  ! -----------------------------------------------------------------


  function timediff(a_date1,ao_date2) Result(dt)
    ! Returns the time difference (h,m,s,ms) between the time stemps a_start
    ! and ao_end. If ao_end not given, the current time is taken.
    ! Assume time zone doesn't change => ignore the time zone (4th entry). 
    !
    !
    ! INPUT/OUTPUT
    ! ------------
    !  a_date1 : INTEGER(8) - in 
    !            first time stemp
    !  ao_date2: INTEGER(8) - in - optional
    !            second time stemp
    !            if not present, current time is used.
    !  diff    : INTEGER - out
    !            diff(1) = hours, diff(2) = minutes, diff(3) = seconds
    !
    ! DESCRIPTION
    ! -----------
    ! Structure of dates are identical to the structure of the intrinsic
    ! subroutine date_and_time(values=date)
    !   date(1) = year , date(2) = month , date(3) = day , date(4) = timezone
    !   date(5) = hour , date(6) = min.  , date(7) = sec. , date(8) = msec.
    !
    implicit none
    integer,intent(in) :: a_date1(:)
    integer,intent(in),optional :: ao_date2(:)
    integer :: dt(4)
    
    integer :: date1(8),date2(8)
    double precision :: dms

    double precision, parameter :: hr  = 1.D3*60**2   &
                                 ,minu = 1.D3*60      &
                                 , sec = 1.D3      


    if (size(a_date1)/=8)  stop " ERROR(timeidff): size of argument #1 /= 8"

    date1 = a_date1

    ! secondd (optional argument)
    call date_and_time(values=date2)    
    if (present(ao_date2)) then
      if (size(ao_date2)/=8)    &
        stop " ERROR(timeidff): size of argument #1 /= 8"
      date2 = ao_date2
    end if


    dt = 0
    ! difference in years,months,days,hours in hours
    dt(1) = daydiff(date1(1:3),date2(1:3))
    dt(1) = dt(1)*24 + date2(5)-date1(5)
    ! get difference in minutes
    dt(2) = date2(6)-date1(6)
    ! get difference in seconds,milliseconds
    dt(3) = date2(7)-date1(7)
    dt(4) = date2(8)-date1(8)

    ! complete date difference in ms
    dms = dt(1) * hr + dt(2) * minu + dt(3) * sec + dt(4)

    ! store difference
    ! hours
    dt(1) = int( dms/hr )
    dms   = mod( dms,hr )
    ! minutes
    dt(2) = int( dms/minu )
    dms   = mod( dms,minu )
    ! seconds
    dt(3) = int( dms/sec )
    dms   = mod( dms,sec )
    ! milliseconds
    dt(4) = int( dms )
    return
  end function timediff

  
  pure integer function daydiff(a_date1,a_date2) Result(days)
    ! calculate number of days between two dates
    !
    ! INPUT
    ! ------
    !  a_date1 : INTEGER(3) - in
    !            year, month, days
    !  a_date2 : INTEGER(3) - in
    !            year, month, days
    !
    implicit none
    integer,intent(in) :: a_date1(3),a_date2(3)
    ! days per month (ignore February anomality)
    integer :: monthday(12)

    monthday = (/31,28,31,30,31,30,31,31,30,31,30,31/)
    
    days = a_date2(3) - a_date1(3)
    days = days + sum( monthday(1:a_date2(2)-1) )   &
                - sum( monthday(1:a_date1(2)-1) )

    days = days + (a_date2(1)-a_date1(1))*365
  end function daydiff


  subroutine timeinfo(a_t,ao_fid,ao_c)
    ! print time information of all time points
    ! and print time difference between the first
    ! and the last one.
    !
    ! INPUT
    ! -----
    !  a_t    : INTEGER(8,*) - in
    !           1. index: time informations (as in date_and_time(values=..)
    !  ao_fid : INTEGER - in - optional
    !           pipe/file ID
    !  ao_c   : CHARACTER - in - optional
    !           leading character of each line printed
    !
    implicit none
    integer,intent(in) :: a_t(1:,1:)
    integer,intent(in),optional :: ao_fid
    character,intent(in),optional :: ao_c

    integer :: i,dt(4), fid, nt
    character(2) :: c

    fid = STDOUT
    if ( present(ao_fid) )  fid = ao_fid
    c = ""
    if ( present(ao_c) )      c = ao_c

    nt = min(size(a_t,2),100)  ! no more than 100 time points
    dt = timediff( a_t(:,1) , a_t(:,nt) )

  
    write(fid,'(a,2(/,2a))') c,  &
        c,"Time information",    &
        c,"----------------"

    write(fid,'(2a,2(i2.2,a),i4,3(a,i2.2),a,i3.3)')             &
        c,"Start      : ",                                      &
        a_t(3,1),".",a_t(2,1),".",a_t(1,1)," @ ",               &
        a_t(5,1),":",a_t(6,1),":",a_t(7,1),".",a_t(8,1)

    ! time points in between
    do i=2,nt-1
      write(fid,'(2a,i2.2,a,2(i2.2,a),i4,3(a,i2.2),a,i3.3)')    &
          c,"TimePoint",i,": ",                                 &
          a_t(3,i),".",a_t(2,i),".",a_t(1,i)," @ ",             &
          a_t(5,i),":",a_t(6,i),":",a_t(7,i),".",a_t(8,i)
    end do


    write(fid,'(2a,2(i2.2,a),i4,3(a,i2.2),a,i3.3)')             &
        c,"End        : ",                                      &
        a_t(3,nt),".",a_t(2,nt),".",a_t(1,nt)," @ ",            &
        a_t(5,nt),":",a_t(6,nt),":",a_t(7,nt),".",a_t(8,nt)
    write(fid,'(2a,i8,a,2(i3.2,a),i4,a)')                       &
          c,"Elapsed    : "                                     &
          ,dt(1),"h",dt(2),"min",dt(3),"s",dt(4),"ms"

  end subroutine timeinfo


end module basics

module constants
  !
  ! -----------------------------------------------------------------------
  ! |  Basic constants for precision, numbers, and std input/output       |
  ! |  physical constants from NIST                                       |
  ! |                                                                     |
  ! |  written by Son, Sang-Kil in Feb. 2004                              |
  ! |  rewritten by Son, Sang-Kil in Mar. 2005 adding physical constants  |
  ! |  small modifiaction by Stefan Pabst in April 2011                   |
  ! -----------------------------------------------------------------------
  !
  ! This module contains no functions or subroutines.
  !
  ! Version info: $Id: Constants.f90 435 2011-11-17 13:39:21Z spabst $
  ! ------------------------------------------------------------------
  !
  implicit none

  type :: t_string
    character(200) :: txt    ! clen=200
  end type t_string


  ! precision : double precision
  ! ---------------------------------
  integer, parameter :: dblprec = selected_real_kind(15)
  integer, parameter :: sglprec = kind(1.0)
  integer, parameter :: clen = 200  ! my default length of a string
  integer, parameter,private :: dp = dblprec
  real(dp), parameter :: EPS = 1.0e-15_dp
  
  ! numbers
  ! ---------------------------------
  real(dp), parameter :: ZERO  = 0.0_dp
  real(dp), parameter :: ONE   = 1.0_dp
  real(dp), parameter :: TWO   = 2.0_dp
  real(dp), parameter :: THREE = 3.0_dp
  real(dp), parameter :: FOUR  = 4.0_dp
  real(dp), parameter :: HALF  = 0.5_dp
  real(dp), parameter :: PI = acos(-ONE)  !3.141592653589793_dp
  complex(dp), parameter :: IMG = ( ZERO, ONE )
  
  
  ! standard input/output parameters
  ! ---------------------------------
  integer, parameter :: STDIN  = 5      ! unit number of standard input
  integer, parameter :: STDOUT = 6      ! unit number of standard output
  integer, parameter :: STDERR = 0      ! unit number of standard error
  
  
  ! physical constants from NIST
  !   ref) Mohr & Taylor, J. Phys. Chem. Ref. Data 28, 1713 (1999)
  !        Mohr & Taylor, Rev. Mod. Phys. 72, 351 (2000)
  !   E: electron, P: proton, and N: neutron
  ! --------------------------------------------------------------------
  real(dp), parameter :: SPEED_C_SI = 2.99792458e8_dp       ! m/s (exact)
  real(dp), parameter :: CHARGE_P_SI = 1.602176462e-19_dp   ! C
  real(dp), parameter :: CHARGE_E_SI = CHARGE_P_SI
  real(dp), parameter :: MASS_E_SI = 9.10938188e-31_dp      ! kg
  real(dp), parameter :: MASS_P_SI = 1.67262158e-27_dp      ! kg
  real(dp), parameter :: MASS_N_SI = 1.67492715e-27_dp      ! kg
  real(dp), parameter :: AMU_SI = 1.66053873e-27_dp         ! kg
  real(dp), parameter :: PLANCK_H_SI = 6.62606876e-34_dp    ! J s
  real(dp), parameter :: BOLTZMANN_K_SI = 1.3806503e-23_dp  ! J/K, k = R/NA
  real(dp), parameter :: AVOGADRO_NA_SI = 6.02214199e23_dp  ! 1/mol
  real(dp), parameter :: GRAVITY_G_SI = 6.673e-11_dp        ! m^3 / kg s^2



  ! atomic units from NIST inspired by Burnus's diploma (2004)
  !   Atomic units defined by h_bar = 1, m_e = 1, e = 1, and a0 = 1.
  !   corollary) epsilon0 = 1/4PI, and E_h = e^2 / 4PI epsilon0 a0 = 1
  !
  ! a.u. alpha : 1/(4 PI epsilon0) e^2/hbar/c ->  1/c (in atomic units)
  ! a.u. of charge: e = 1 a.u. = 1.602176462e-19 C   (CHARGE_E_SI)
  ! a.u. of mass:   m_e = 1 a.u. = 9.10938188e-31 kg (MASS_E_SI)
  ! a.u. of action: h_bar = h / 2PI = 1 a.u.         (PLANCK_H_SI / TWO / PI)
  ! a.u. of length: a0 = h_bar^2 / 4PI epsilon0 m_e e^2 = 1 a.u. (bohr)
  ! a.u. of energy: E_h = e^2 / 4PI epsilon0 a0 = 1.a.u. (hartree)
  ! a.u. of time                        := h_bar / E_h
  ! a.u. of force                       := E_h / a0
  ! a.u. of velocity                    := a0 E_h / h_bar
  ! a.u. of momentum                    := h_bar / a0
  ! a.u. of current                     := e E_h / h_bar
  ! a.u. of charge density              := e / a0^3
  ! a.u. of electric potential          := E_h / e
  ! a.u. of electric field              := E_h / e a0
  ! a.u. of electric field gradient     := E_h / e a0^2
  ! a.u. of electric dipole moment      := e a0
  ! a.u. of electric quadrupole moment  := e a0^2
  ! a.u. of electric polarizability     := e^2 a0^2 / E_h
  ! a.u. of magnetic field              := h_bar / e a0^2
  ! --------------------------------------------------------------------------
  real(dp), parameter :: AU_ALPHA  = 7.2973525376E-3_dp       ! alpha ~ 1/137
  real(dp), parameter :: AU_CHARGE = CHARGE_E_SI              ! 1.602e-19 C
  real(dp), parameter :: AU_MASS   = MASS_E_SI                ! 9.109e-31 kg
  real(dp), parameter :: AU_ACTION = PLANCK_H_SI / TWO / PI   ! 1.055e-34 J s
  real(dp), parameter :: AU_LENGTH = 0.5291772083e-10_dp    ! m
  real(dp), parameter :: AU_ENERGY = 4.35974381e-18_dp      ! J
  real(dp), parameter :: AU_TIME   = 2.418884326500e-17_dp  ! sec
  real(dp), parameter :: AU_FORCE  = 8.23872181e-8_dp       ! N
  real(dp), parameter :: AU_VELOCITY = 2.1876912529e6_dp    ! m/s
  real(dp), parameter :: AU_MOMENTUM = 1.99285151e24_dp     ! kg m/s
  real(dp), parameter :: AU_CURRENT  = 6.62361753e-3_dp     ! A
  real(dp), parameter :: AU_CHARGE_DENSITY   = 1.081202285e12_dp    ! C/m^3
  real(dp), parameter :: AU_E_POTENTIAL      = 27.2113834_dp        ! V
  real(dp), parameter :: AU_E_FIELD          = 5.14220624e11_dp     ! V/m
  real(dp), parameter :: AU_E_FIELD_GRAD     = 9.71736153e21_dp     ! V/m^2
  real(dp), parameter :: AU_E_DIPOLE_MOMENT  = 8.47835267e-30_dp    ! C m
  real(dp), parameter :: AU_E_QUAD_MOMENT    = 4.48655100e-40_dp    ! C m^2
  real(dp), parameter :: AU_E_POLARIZABILITY = 1.648777251e-41_dp
                                                                  ! C^2 m^2 / J
  real(dp), parameter :: AU_MAGNETIC_FIELD   = 2.35051735e5_dp      ! T
  real(dp), parameter :: AU_INTENSITY = AU_ENERGY / AU_TIME / AU_LENGTH**2 
                                                                  ! W/m**2
  real(dp), parameter :: AU_INTENSITY_EMF = AU_INTENSITY / (8*PI*AU_ALPHA) 
                                                                  ! W/m**2

  ! a.u. conversion
  ! ---------------------------------
  real(dp), parameter :: AU2J   = AU_ENERGY        ! 1 a.u. = 4.3597e-18 J
  real(dp), parameter :: AU2EV  = AU_E_POTENTIAL   ! 1 a.u. = 27.2114 eV
  real(dp), parameter :: AU2M   = AU_LENGTH        ! 1 a.u. = 5.29e-11 m
  real(dp), parameter :: AU2ANG = AU_LENGTH * 1.0e10_dp
                                                   ! 1 a.u. = 0.529 angstrom
  real(dp), parameter :: AU2KG  = AU_MASS          ! 1 a.u. = 9.109e-31 kg
  real(dp), parameter :: AU2SEC = AU_TIME          ! 1 a.u. = 2.4189e-17 sec
  real(dp), parameter :: AU2ATTOSEC = AU_TIME * 1.0e18_dp
                                                     ! 1 a.u. = 24.189 attosec.
  real(dp), parameter :: AU2V_M = AU_E_FIELD

  ! a.u. reverse conversion
  ! ---------------------------------
  real(dp), parameter :: J2AU   = ONE / AU2J           ! 1 J = 2.2937e17 a.u.
  real(dp), parameter :: EV2AU  = ONE / AU2EV          ! 1 eV = 0.036749 a.u.
  real(dp), parameter :: M2AU   = ONE / AU2M           ! 1 m = 1.8897e10 a.u.
  real(dp), parameter :: ANG2AU = ONE / AU2ANG         ! 1 angs. = 1.8897 a.u.
  real(dp), parameter :: KG2AU  = ONE / AU2KG          ! 1 kg = 1.0978e30 a.u.
  real(dp), parameter :: SEC2AU = ONE / AU2SEC         ! 1 sec = 4.134e16 a.u.
  real(dp), parameter :: ATTOSEC2AU = ONE / AU2ATTOSEC ! 1 asec = 0.04134 a.u.
  real(dp), parameter :: V_M2AU = ONE / AU_E_FIELD

  ! speed of light: c = 1 / sqrt( epsilon0 mu0 ) = 2.9979e8 m/s = 137.04 a.u.
  !   electric constant: epsilon0 = 8.854187817e-12 F / m (exact)
  !   magnetic constant: mu0 = 4PI * e-7 = 12.566370614e-7 N / A^2 (exact)
  real(dp), parameter :: SPEED_C_AU = SPEED_C_SI * M2AU / SEC2AU
  

  ! physical constant conversion
  ! ---------------------------------
  real(dp), parameter :: CAL2J = 4.184_dp           ! 1 cal = 4.184 J
  real(dp), parameter :: KCAL2J = CAL2J * 1.0e3_dp  ! 1 kcal = 4184 J
  real(dp), parameter :: EV2J = CHARGE_E_SI           ! 1 eV = 1.602e-19 J
  real(dp), parameter :: EV2KCALMOL = EV2J / KCAL2J * AVOGADRO_NA_SI
                                                        ! 1 eV = 23.06 kcal/mol
  real(dp), parameter :: HARTREE2J = AU2J             ! 1 a.u = 4.3597e-18 J
  real(dp), parameter :: HARTREE2EV = AU2EV           ! 1 a.u = 27.2114 eV
  real(dp), parameter :: HARTREE2KCALMOL = HARTREE2EV * EV2KCALMOL
                                                        ! 1 a.u = 627.5 kcal/mol
  real(dp), parameter :: J2CAL     = ONE / CAL2J      ! 1 J = 0.239 cal
  real(dp), parameter :: J2KCAL    = ONE / KCAL2J     ! 1 J = 2.39e-4 kcal
  real(dp), parameter :: J2EV      = ONE / EV2J       ! 1 J = 6.24e18 eV
  real(dp), parameter :: J2HARTREE = ONE / HARTREE2J  ! 1 J = 2.29e17 hartree


  ! energy relations: E = h nu = h c sigma = h c / lambda = k T
  !   1 eV -> 2.419884e14 Hz -> 8065.5410 cm^-1 (1239.84244 nm) -> 11604.45 K
  ! --------------------------------------------------------------------------
  real(dp), parameter :: EV2HZ = EV2J / PLANCK_H_SI   ! 1 eV = 2.419884e14 Hz
  real(dp), parameter :: EV2WAVENUMBER = EV2HZ / SPEED_C_SI / 100.0_dp
  real(dp), parameter :: EV2KAYSER = EV2WAVENUMBER    ! 1 eV = 8065.5410 cm^-1
                                                        !     -> 1239.84244 nm
  real(dp), parameter :: EV2K = EV2J / BOLTZMANN_K_SI ! 1 eV = 11604.45 K
end module constants


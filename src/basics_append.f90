module basics_append
  !
  !**PURPOSE
  !  -------
  !  This module contains only routines that extend vectors and arrays of type
  !  integer, real, double, complex, double complex.
  !  The array can have the allocatable or pointer artibute.
  !
  !  -> (sbr) append(a,b) : append b on a 
  !                         for further options see routine header
  !
  !**AUTHOR INFORMATION
  !  ------------------
  !  written by Stefan Pabst in June 2011
  !
  !**VERSION INFO
  !  ------------
  !  $Id: basics.f90 21 2011-10-11 22:05:56Z paule $
  !-------------------------------------------------------------------------
  !
  use constants,  dp=>dblprec,sp=>sglprec
  implicit none

  interface append_allo
    module procedure  append_i_1D_allo ,append_i_2D_allo    &
                     ,append_rs_1D_allo,append_rs_2D_allo   &
                     ,append_rd_1D_allo,append_rd_2D_allo   &
                     ,append_cs_1D_allo,append_cs_2D_allo   &
                     ,append_cd_1D_allo,append_cd_2D_allo 
  end interface append_allo

  interface append_ptr
    module procedure  append_i_1D_ptr ,append_i_2D_ptr      &
                     ,append_rs_1D_ptr,append_rs_2D_ptr     &
                     ,append_rd_1D_ptr,append_rd_2D_ptr     &
                     ,append_cs_1D_ptr,append_cs_2D_ptr     &
                     ,append_cd_1D_ptr,append_cd_2D_ptr    
  end interface append_ptr
contains



  ! -------------------------------
  ! -------------------------------
  ! |  Append allocatable fields  |
  ! -------------------------------
  ! -------------------------------

  subroutine append_i_1D_allo(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - INTEGER(:) - INOUT
    !              field to be extended
    !  a_add     - INTEGER(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    integer, intent(inout),allocatable :: a_main(:)
    integer, intent(in) :: a_add(:)
    
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    integer, allocatable :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.allocated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_i_1D_allo
  subroutine append_i_2D_allo(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - INTEGER(:,:) - INOUT
    !              field to be extended
    !  a_add     - INTEGER(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    integer, intent(inout),allocatable :: a_main(:,:)
    integer, intent(in) :: a_add(:,:)
    
    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    integer, allocatable :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.allocated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_i_2D_allo

  ! ----------------------------------

  subroutine append_rs_1D_allo(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    real(sp), intent(inout),allocatable :: a_main(:)
    real(sp), intent(in) :: a_add(:)
    
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(sp), allocatable :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.allocated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rs_1D_allo
  subroutine append_rs_2D_allo(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:,:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    real(sp), intent(inout),allocatable :: a_main(:,:)
    real(sp), intent(in) :: a_add(:,:)
    
    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(sp), allocatable :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.allocated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rs_2D_allo

  ! ----------------------------------

  subroutine append_rd_1D_allo(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    real(dp), intent(inout),allocatable :: a_main(:)
    real(dp), intent(in) :: a_add(:)
    
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(dp), allocatable :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.allocated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rd_1D_allo
  subroutine append_rd_2D_allo(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:,:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    real(dp), intent(inout),allocatable :: a_main(:,:)
    real(dp), intent(in) :: a_add(:,:)
    
    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(dp), allocatable :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.allocated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rd_2D_allo

  ! ----------------------------------

  subroutine append_cs_1D_allo(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    complex(sp), intent(inout),allocatable :: a_main(:)
    complex(sp), intent(in) :: a_add(:)
    
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(sp), allocatable :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.allocated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cs_1D_allo
  subroutine append_cs_2D_allo(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:,:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    complex(sp), intent(inout),allocatable :: a_main(:,:)
    complex(sp), intent(in) :: a_add(:,:)
    
    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(sp), allocatable :: aux(:,:)
    
    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.allocated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cs_2D_allo

  ! ----------------------------------

  subroutine append_cd_1D_allo(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    complex(dp), intent(inout),allocatable :: a_main(:)
    complex(dp), intent(in) :: a_add(:)
    
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(dp), allocatable :: aux(:)
    
    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.allocated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cd_1D_allo
  subroutine append_cd_2D_allo(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:,:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    complex(dp), intent(inout),allocatable :: a_main(:,:)
    complex(dp), intent(in) :: a_add(:,:)
    
    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(dp), allocatable :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.allocated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cd_2D_allo




  !! -------------------------------
  !! -------------------------------
  !!  |  Append pointer fields  |
  !! -------------------------------
  !! -------------------------------



  subroutine append_i_1D_ptr(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - INTEGER(:) - INOUT
    !              field to be extended
    !  a_add     - INTEGER(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    integer, intent(inout),pointer :: a_main(:)
    integer, intent(in) :: a_add(:)

    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    integer, pointer :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.associated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_i_1D_ptr
  subroutine append_i_2D_ptr(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - INTEGER(:,:) - INOUT
    !              field to be extended
    !  a_add     - INTEGER(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    integer, intent(inout),pointer :: a_main(:,:)
    integer, intent(in) :: a_add(:,:)
    
    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    integer, pointer :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.associated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_i_2D_ptr

  ! ----------------------------------

  subroutine append_rs_1D_ptr(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    real(sp), intent(inout),pointer :: a_main(:)
    real(sp), intent(in) :: a_add(:)

    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(sp), pointer :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.associated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rs_1D_ptr
  subroutine append_rs_2D_ptr(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:,:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    real(sp), intent(inout),pointer :: a_main(:,:)
    real(sp), intent(in) :: a_add(:,:)

    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(sp), pointer :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.associated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rs_2D_ptr

  ! ----------------------------------

  subroutine append_rd_1D_ptr(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    real(dp), intent(inout),pointer :: a_main(:)
    real(dp), intent(in) :: a_add(:)

    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(dp), pointer :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.associated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rd_1D_ptr
  subroutine append_rd_2D_ptr(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - REAL(:,:) - INOUT
    !              field to be extended
    !  a_add     - REAL(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    real(dp), intent(inout),pointer :: a_main(:,:)
    real(dp), intent(in) :: a_add(:,:)

    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    real(dp), pointer :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.associated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_rd_2D_ptr

  ! ----------------------------------

  subroutine append_cs_1D_ptr(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    complex(sp), intent(inout),pointer :: a_main(:)
    complex(sp), intent(in) :: a_add(:)

    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(sp), pointer :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.associated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cs_1D_ptr
  subroutine append_cs_2D_ptr(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:,:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none
    
    complex(sp), intent(inout),pointer :: a_main(:,:)
    complex(sp), intent(in) :: a_add(:,:)

    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(sp), pointer :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.associated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cs_2D_ptr

  ! ----------------------------------

  subroutine append_cd_1D_ptr(a_main,a_add,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:) - IN
    !              part added to a_main
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    complex(dp), intent(inout),pointer :: a_main(:)
    complex(dp), intent(in) :: a_add(:)

    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(dp), pointer :: aux(:)

    integer :: m1
    integer :: a1
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    ! size of field to add
    a1 = size(a_add,1)

    if ( .not.associated(a_main) ) then
      ! a_main is empty
      allocate(a_main(a1))
      m1 = 0
    else
      ! extend field
      m1 = size(a_main,1)
      allocate(aux(m1))
      aux = a_main
      deallocate(a_main)
      allocate(a_main(m1+a1))
      ! copy back old part
      if ( append ) then
        a_main(1:m1) = aux
      else
        a_main(a1+1:) = aux
      end if
      deallocate(aux)
    end if
   
    ! add new part
    if ( append ) then
      a_main(m1+1:) = a_add
    else
      a_main(1:a1) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cd_1D_ptr
  subroutine append_cd_2D_ptr(a_main,a_add,ao_dim,ao_append,ao_info)
    !
    !**DESCRIPTION
    !  -----------
    !  Append a_add to a_main
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_main    - COMPLEX(:,:) - INOUT
    !              field to be extended
    !  a_add     - COMPLEX(:,:) - IN
    !              part added to a_main
    !  ao_dim    - INTEGER - IN - OPTIONAL
    !              index to be extended
    !               = 2 [default]
    !  ao_append - LOGICAL - IN - OPTIONAL
    !               = TRUE  : append [DEFAULT]
    !               = FALSE : in front
    !  ao_info   - LOGICAL - OUT - OPTIONAL
    !              true when exit successfully
    !
    implicit none

    complex(dp), intent(inout),pointer :: a_main(:,:)
    complex(dp), intent(in) :: a_add(:,:)

    integer, intent(in), optional :: ao_dim
    logical, intent(out),optional :: ao_info
    logical, intent(in),optional :: ao_append

    complex(dp), pointer :: aux(:,:)

    integer :: m1,m2
    integer :: a1,a2
    integer :: n1,n2
    integer :: d
    logical :: append


    if ( present(ao_info) ) then
      ao_info = .false.
    end if

    append = .true.
    if ( present(ao_append) ) append = ao_append

    d=2
    if ( present(ao_dim) ) d=ao_dim
    if ( d<1 .or. d>2) return

    ! size of field to add
    a1 = size(a_add,1)
    a2 = size(a_add,2)

    if ( .not.associated(a_main) ) then

      ! a_main is empty
      m1 = 0
      m2 = 0
      n1 = a1
      n2 = a2

      allocate(a_main(a1,a2))

    else

      m1 = size(a_main,1)
      m2 = size(a_main,2)

      if ( d==1 ) then
        n1 = m1 + a1
        n2 = m2
        if ( a2 /= m2 ) return
      else
        n1 = m1
        n2 = m2 + a2
        if ( a1 /= m1 ) return
      end if

      allocate(aux(m1,m2))
      aux = a_main

      ! extend field
      deallocate(a_main)
      allocate(a_main(n1,n2))

      ! copy back old part
      if ( append ) then
        if ( d==1 ) a_main(1:m1,:) = aux
        if ( d==2 ) a_main(:,1:m2) = aux
      else
        if ( d==1 ) a_main(a1+1:,:) = aux
        if ( d==2 ) a_main(:,a2+1:) = aux
      end if
      deallocate(aux)

    end if
   
    ! add new part
    if ( append ) then
      if ( d==1 ) a_main(m1+1:,:) = a_add
      if ( d==2 ) a_main(:,m2+1:) = a_add
    else
      if ( d==1 ) a_main(1:a1,:) = a_add
      if ( d==2 ) a_main(:,1:a2) = a_add
    end if

    if ( present(ao_info) ) then
      ao_info = .true.
    end if
  end subroutine append_cd_2D_ptr

end module basics_append

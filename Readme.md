
###Synopsis:
**trap.x** file-name [type] [column]  [action:action:...]  [para0:para1:...]

**type**  
= 1 : real function [1 column]  
= 2 : complex fuction [2 columns]  

**column** = i[:j]  
read data columns i to j [default: i=2]  
i/j=-1: second/last column  
j=i:    if only i is provided

**Action**  
 = 01 : FFT{fct} (forward) [default]  
 = 02 : FFT{fct} (backward)  
 = 03 : FFT{fct} (forward,  FFT domain centerd at 0)  
 = 04 : FFT{fct} (backward, FFT domain centerd at 0)  
 = 11 : d/dx fct(x)  
 = 12 : d^2/dx^2 fct(x)  
 = 21 : Integrate fct(x) from x=a to x=b; parameter: a:b  
 = 30 :  a  * fct(x); parameter: Re(a):Im(a)  
 = 31 :  x  * fct(x)  
 = 32 : x^2 * fct(x)  
 = 38 : fct(x)/x  
 = 39 : fct(x)/x^2  
 = 41 : |fct(x)|  
 = 42 : fct(x)^2  
 = 43 : fct(x)^3  
 = 51 : fct(x) * exp(-(x-x0)^2/w^2); parameter: x0:w  
 = 52 : fct(x) * exp(-(x-x0)/w); parameter: x0:w  
 = 60 :  a + fct(x); parameter: Re(a):Im(a)  

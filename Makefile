#!/bin/sh
#
name=trap

CO=ifort -module $(BIN)/
FLAG=-fast #-xT 
LIB=-L$(HOME)/lib/ -ldfftpack
CC=$(CO) $(FLAG)

SRC=src
BIN=bin
MOD=$(BIN)/constants.o $(BIN)/basics.o $(BIN)/basics_append.o $(BIN)/inout.o $(BIN)/SplineInterpol.o $(BIN)/fft.o


.PHONY: clean all init install uninstall
.SUFFIXES: .o .mod
.SECONDARY:  #prevents *.o files from automatic deletion 

all : init $(BIN)/$(name).x

$(BIN)/%.o:  $(SRC)/%.f90
	$(CC) -c $< -o $@

$(BIN)/%.x:  $(MOD) $(SRC)/%.f90
	$(CC) $^ -o $@ $(LIB) 


init :
	@mkdir -p $(BIN)/

install :
	ln -s $$(pwd)/$(BIN)/$(name).x $(HOME)/bin/

uninstall :
	rm -f $(name).x $(BIN)/* $(HOME)/bin/$(name).x


clean : init
	rm -f $(BIN)/* 

